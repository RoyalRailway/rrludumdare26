#ifndef SFML_RESOURCE_PATH_H
	#define SFML_RESOURCE_PATH_H
	#include <string>

	namespace sf
	 {
		std::string resourcePath();
	 }
#endif
