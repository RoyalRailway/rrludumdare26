//! @author Adam Emil Skoog, Royal Railway HB
//! @date   2013-04-28

#include "maths.h"

namespace flip
 {/*
	bool doCircleAndPointCollide(const sf::CircleShape &a,const sf::Vector2f &b)
	 {
		return ((a.getPosition().x - b.x) * (a.getPosition().x - b.x) +
			(a.getPosition().y - b.y) * (a.getPosition().y - b.y) <=
			a.getRadius() * a.getRadius());
	 }

	bool doCircleAndLineCollide(const sf::CircleShape &a,const sf::Vector2f &b1,const sf::Vector2f &b2)
	 {
		if (doCircleAndPointCollide(a,b1))
			return true;
		
		if (doCircleAndPointCollide(a,b2))
			return true;
		
		const float ANGLE(atan2(b1.x - b2.x,b1.y - b2.y));
		const sf::Vector2f A(a.getPosition().x + std::cos(ANGLE) * a.getRadius(),a.getPosition().y - std::sin(ANGLE) * a.getRadius()),
						   B(a.getPosition().x - std::cos(ANGLE) * a.getRadius(),a.getPosition().y + std::sin(ANGLE) * a.getRadius());
		
		const sf::Vector2f P[] = {A,B,b1,b2};

		if (doLinesIntersect(P))
			return true;

		return false;
	 }
*/ }
