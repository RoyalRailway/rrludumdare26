//! @author Adam Emil Skoog, Royal Railway HB
//! @date   2013-04-28

#ifndef RRLudumDare26_maths_h
	#define RRLudumDare26_maths_h
	#include <SFML/Graphics.hpp>
	#include <cmath>

	namespace flip
	 {
		/*
		 * Calculates the distance between two points.
		 */
		template<class T> inline const float pointDistance(const sf::Vector2<T> &a,const sf::Vector2<T> &b)
		 {
			 // The reason I am using pow rather than std::pow, is that it
			 // will otherwise not compile for Windows, for some reason.
			 return std::sqrt(pow(b.x - a.x,T(2)) +
							  pow(b.y - a.y,T(2)));
		 }

		/*
		 * Calculates the angle between two points.
		 */
		template<class T> const float lineAngle(const sf::Vector2<T> &a,const sf::Vector2<T> &b)
		 {
			 return std::atan2(b.x - a.x,b.y - a.y);
		 }

		/*
		 * Converts radians to degrees.
		 */
		template<class T> const T radToDeg(const T &value)
		 {
			return value * 57.2957795;
		 }

		/*
		 * Converts degrees to radians.
		 */
		template<class T> const T degToRad(const T &value)
		 {
			return value / 57.2957795;
		 }
/*
		template<class T> inline bool doLinesIntersect(const sf::Vector2<T> p[])
		 {
			const T X_1   (p[0].x - p[1].x),
			X_2   (p[2].x - p[3].x),
			STEP_1((p[0].y - p[1].y) / ((X_1 == T(0)) ? T(1) : X_1)),
			STEP_2((p[2].y - p[3].y) / ((X_2 == T(0)) ? T(1) : X_2)),
			M_1   (STEP_1 * p[1].x - p[1].y),
			M_2   (STEP_2 * p[3].x - p[3].y),
			X     ((M_1 - M_2) / (STEP_1 - STEP_2)),
			Y     (round(X * STEP_1 - M_1)),
			XX    (round((M_1 - M_2) / (STEP_1 - STEP_2)));
			
			return (XX >= round(std::min(p[0].x,p[1].x)) && XX <= round(std::max(p[0].x,p[1].x)) &&
					XX >= round(std::min(p[2].x,p[3].x)) && XX <= round(std::max(p[2].x,p[3].x)) &&
					Y >= round(std::min(p[0].y,p[1].y)) && Y <= round(std::max(p[0].y,p[1].y)) &&
					Y >= round(std::min(p[2].y,p[3].y)) && Y <= round(std::max(p[2].y,p[3].y)));
		 }

		bool doCircleAndPointCollide(const sf::CircleShape &a,const sf::Vector2f &b);
		bool doCircleAndLineCollide(const sf::CircleShape &a,const sf::Vector2f &b1,const sf::Vector2f &b2);
*/	 }
#endif
