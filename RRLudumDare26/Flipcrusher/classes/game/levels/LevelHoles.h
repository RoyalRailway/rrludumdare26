//! @author Adam Emil Skoog, Royal Railway HB
//! @date   2013-04-27

// Updates:
// 2013-04-28
// 2013-04-29

/*
 * Level where the character runs across the floor,
 * avoiding holes and luring enemies into them.
 * The gameplay sometimes shifts the direction from
 * vertical to horizontal.
 */
#ifndef __RRLudumDare26__LevelHoles__
	#define __RRLudumDare26__LevelHoles__
	#include "Level.h"
	#include "../Object.h"
	#include "../Game.h"
	#include "../../assets/Image.h"
	#include <SFML/Audio.hpp>

	namespace flip
	 {
		class Sound;

		class LevelHoles : public Level
		 {
			private:
				class Character;
				class Enemy;

			public:
				LevelHoles();
				bool isDone() const;
				void reset();
				~LevelHoles();

			protected:
				void onStart();
				void update();
				void draw() const;

			private:
				const unsigned rows, columns, blackDistribution;
				Object ***objects;
				Character *character;
				Enemy *enemies[4];
				sf::Music music;
				float speedLevel;
				Image *imageVignette;
				Sound *soundHit, *soundScream, *soundWarning, *soundWin;
				unsigned countFinish;
				bool win;

				void bake();

				class Character : public Object
				 {
					public:
						explicit Character();
						void update();
						void draw() const;
						const sf::FloatRect getGlobalBounds(const Object::Colour &) const;
						void reset();

						Image shape;
						bool hitBlack, hitWhite, hit;
						LevelHoles *level;
						float scale;
				 };

				class Enemy : public Object
				 {
					public:
						explicit Enemy();
						void update();
						void draw() const;
						void bake();
						void reset();
						const sf::FloatRect getGlobalBounds(const Object::Colour &) const;

						sf::Vector2f speed, positionStart;
						Image shape;
						bool hitBlack, hitWhite, hit, warned;
						LevelHoles *level;

						static Enemy *warner;
						static Image *image;
				 };
		 };
	 }
#endif
