//! @author Adam Emil Skoog, Royal Railway HB
//! @date   2013-04-28

#include "LevelFirst.h"
#include "../Game.h"
#include "../Object.h"
#include "../../../ResourcePath.h"
#include "ManagerLevels.h"
#include <cmath>

#ifdef DEBUG
	#include <iostream>
#endif

namespace flip
 {
	LevelFirst::LevelFirst()
	:
	character("graphics/character32.png"),
	floor("graphics/floorIntro.png"),
	ceiling("graphics/ceilingIntro.png"),
	pedestal("graphics/pedestal.png"),
	device("graphics/device.png"),
	text("",*Game::getFont(),11),
	textSpace("\n\n\n\n\n\n\nspace",*Game::getFont(),11),
	textFull("Could this be..?                                 \n\nThe item I've been seeking for so many years.                          \n\nIf  only  I  could  grab  it  from  the  pedestal...                                \n\nThis  would  make  me  the  master  of  space. "),
	lengthText(textFull.length()),
	state(0),
	soundWarning("sounds/warning.ogg"),
	soundText("sounds/text.ogg"),
	soundClink("sounds/clink.ogg")
	 {
		soundWarning.setVolume(50.f);
		soundWarning.setLoop(true);
		soundText.setVolume(150.f);

		if (!music1.openFromFile(sf::resourcePath() + "sounds/musicPanic.ogg"))
		 {
			#ifdef DEBUG
				std::cout << "Failed to load sound 'sounds/musicPanic.ogg'. Aborting execution." << std::endl;
			#endif
			
			exit(EXIT_FAILURE);
		 }

		music1.setLoop(true);

		character.setPosition
		 (
			character.getLocalBounds().width * -1.2f,
			Game::getWindow()->getSize().y - floor.getLocalBounds().height -
			character.getLocalBounds().height
		 );

		floor.setOrigin(0,floor.getLocalBounds().height);
		floor.setPosition(0,Game::getWindow()->getSize().y);

		ceiling.setPosition(0,0);

		text.setColor(sf::Color::White);
		text.setPosition(Game::getSizeGrid() - 4,ceiling.getLocalBounds().height + Game::getSizeGrid() - 4);

		textSpace.setColor(sf::Color(255,0,0,0));
		textSpace.setPosition(text.getPosition().x + 315.f,text.getPosition().y - 11.f);

		#ifndef __APPLE__
			textSpace.move(-7.f,.0f);
		#endif

		pedestal.setPosition
		 (
			Game::getWindow()->getSize().x / 2 - pedestal.getLocalBounds().width / 2,
			floor.getPosition().y - floor.getLocalBounds().height - pedestal.getLocalBounds().height
		 );

		device.setOrigin(device.getLocalBounds().width,device.getLocalBounds().height);

		device.setPosition
		 (
			Game::getWindow()->getSize().x / 2 + device.getLocalBounds().width / 2,
			.0
		 );
	 }

	void LevelFirst::update()
	 {
		if (state < 6)
			device.setPosition
			 (
				device.getPosition().x,
				pedestal.getPosition().y - 10.f +
				std::sin(Game::getTimer()->getTimeGame() * 3.) * 2.f
			 );

		switch (state)
		 {
			case 0:
			 {
				if (Game::getTimer()->getTimeGlobal() >= 3.)
					++ state;

				break;
			 }

			case 1:
			 {
				const float d(Game::getWindow()->getSize().x * .14f);

				if (character.getPosition().x < d)
				 {
					character.move(Game::getTimer()->getTimeGlobalDelta() * 30.f,.0f);

					if (character.getPosition().x >= d)
					 {
						character.setPosition(d,character.getPosition().y);
						Game::getTimer()->resetTimeGlobal();
						++ state;
					 }
				 }

				break;
			 }

			case 2:
			 {
				if (Game::getTimer()->getTimeGlobal() >= 1.5)
				 {
					Game::getTimer()->resetTimeGlobal();
					++ state;
				 }

				break;
			 }

			case 3:
			 {
				const long l(text.getString().toAnsiString().length());

				if (l < lengthText)
				 {
					if (Game::getTimer()->getTimeGlobal() >= .035)
					 {
						if (textFull[l] != ' ' && textFull[l] != '\n')
							soundText.play();

						text.setString(textFull.substr(0,l + 1));
						Game::getTimer()->resetTimeGlobal();
					 }
				 }
				else
					++ state;

				break;
			 }

			case 4:
			 {
				const double a(255. * Game::getTimer()->getTimeGlobal());

				textSpace.setColor
				 (
					sf::Color
					 (
						255,0,0,
						std::min(255.,round(a / 25.f) * 25.f)
					 )
				 );

				if (a >= 255.)
					++ state;

				break;
			 }

			case 5:
			 {
				if (Game::isInputActive() && sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
				 {
					ManagerLevels::music->stop();
					Object::soundFlip->play();
					Game::setColourClear(sf::Color::White);
					Game::getTimer()->resetTimeGlobal();
					++ state;
				 }

				break;
			 }

			case 6:
			 {
				if (Game::getTimer()->getTimeGlobal() >= 1.)
				 {
					Game::getTimer()->resetTimeGlobal();
					yOldDevice = device.getPosition().y;
					++ state;
				 }

				break;
			 }

			case 7:
			 {
				const float d(floor.getPosition().y - floor.getLocalBounds().height);

				if (device.getPosition().y < d)
				 {
					device.move
					 (
						-80.f * std::max(.0,std::min(1.,Game::getTimer()->getTimeGlobal() - .4)) * Game::getTimer()->getTimeGlobalDelta(),
						130.f * Game::getTimer()->getTimeGlobal() * Game::getTimer()->getTimeGlobalDelta()
					 );
					device.setRotation(std::min(90.f,90.f * ((d - yOldDevice) - (d - device.getPosition().y)) / (d - yOldDevice)));

					if (device.getPosition().y >= d)
					 {
						device.setPosition(device.getPosition().x,d);
						Game::getTimer()->resetTimeGlobal();
						soundClink.play();
						++ state;
					 }
				 }

				break;
			 }

			case 8:
			 {
				const double d(floor.getPosition().y - floor.getLocalBounds().height);

				if (Game::getTimer()->getTimeGlobal() <= 1.)
				 {
					const float s(std::sin(10. - Game::getTimer()->getTimeGlobal() * 10.f));
					device.setRotation(90.f + s * (std::max(.0,20.f - Game::getTimer()->getTimeGlobal() * 30.f)));
					device.setPosition(device.getPosition().x,std::min(d,d - s * std::max(.0,(15.f - Game::getTimer()->getTimeGlobal() * 20.f))));
				 }
				else
				 {
					device.setRotation(90.f);
					device.setPosition(device.getPosition().x,d);
					xOldDevice = device.getPosition().x;
					Game::getTimer()->resetTimeGlobal();
					++ state;
				 }

				break;
			 }

			case 9:
			 {
				if (Game::getTimer()->getTimeGlobal() >= 1.5)
				 {
					Game::getTimer()->resetTimeGlobal();
					++ state;
				 }

				break;
			 }

			case 10:
			 {
				const float d(device.getPosition().x - character.getLocalBounds().width * 1.02f);

				if (character.getPosition().x < d)
				 {
					character.move(30.f * std::min(1.,Game::getTimer()->getTimeGlobal()) * Game::getTimer()->getTimeGlobalDelta(),.0f);

					if (character.getPosition().x >= d)
					 {
						character.setPosition(d,character.getPosition().y);
						Game::getTimer()->resetTimeGlobal();
						++ state;
					 }
				 }

				break;
			 }

			case 11:
			 {
				if (Game::getTimer()->getTimeGlobal() >= 1.8)
				 {
					Game::getTimer()->resetTimeGlobal();
					music1.play();
					soundWarning.play();
					++ state;
				 }

				break;
			 }

			case 12:
			 {
				if (Game::getTimer()->getTimeGlobal() >= 1.4)
				 {
					Game::getTimer()->resetTimeGlobal();
					soundWarning.play();
					++ state;
				 }

				break;
			 }

			case 13:
			 {
				const float d(Game::getWindow()->getSize().x * 1.1f);

				if (character.getPosition().x < d)
				 {
					character.move(110.f * std::min(1.,Game::getTimer()->getTimeGlobal()) * Game::getTimer()->getTimeGlobalDelta(),.0f);

					if (character.getPosition().x >= xOldDevice)
					 {
						device.setPosition(character.getPosition().x * 1.05f,device.getPosition().y);
						device.move(.0f,-6.f * Game::getTimer()->getTimeGlobalDelta());
						const float s(std::min(1.,std::max(.0,1.f - (Game::getTimer()->getTimeGlobal() - 1.) * 2.f)));
						device.setScale(s,s);
					 }
				 }
				else
				 {
					if (Game::isInputActive() && sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
					 {
						Game::getTimer()->resetTimeGlobal();
						Game::setColourClear(sf::Color::Black);
						soundWarning.stop();
						music1.stop();
						Object::soundFlip->play();
						++ state;
					 }
				 }

				break;
			 }

			case 14:
			 {
				if (Game::getTimer()->getTimeGlobal() >= 1.5)
				 {
					Game::getTimer()->resetTimeGlobal();
					++ state;
				 }

				break;
			 }

			case 15:
			 {
				const double a(255. * Game::getTimer()->getTimeGlobal() / 2.);

				textSpace.setColor
				 (
					sf::Color
					 (
						255,0,0,
						255. - std::min(255.,round(a / 25.f) * 25.f)
					 )
				 );

				/*
				soundWarning.setVolume(50. - std::min(50.,50. * Game::getTimer()->getTimeGlobal() / 3.));
				music1.setVolume(soundWarning.getVolume() * 2.);
				*/

				if (a >= 255.)
				 {
					//soundWarning.stop();
					++ state;
				 }

				break;
			 }
		 }
	 }

	void LevelFirst::draw() const
	 {
		if (state < 14)
		 {
			Game::getWindow()->draw(ceiling);
			Game::getWindow()->draw(floor);
			Game::getWindow()->draw(character);

			if (state < 6)
				Game::getWindow()->draw(pedestal);

			Game::getWindow()->draw(device);

			if (state >= 3 && state <= 5)
				Game::getWindow()->draw(text);

			if (state >= 12)
			 {
				sf::RectangleShape s(sf::Vector2f(Game::getWindow()->getSize().x,Game::getWindow()->getSize().y));
				s.setPosition(.0f,.0f);
				s.setFillColor(sf::Color(255,0,0,60.f + 40.f * std::sin(Game::getTimer()->getTimeGame() * 9.7)));
				Game::getWindow()->draw(s);
			 }
/*
			if (state == 14)
			 {
				sf::RectangleShape s(sf::Vector2f(Game::getWindow()->getSize().x,Game::getWindow()->getSize().y));
				const double a(255. * Game::getTimer()->getTimeGlobal() / 3.);

				s.setFillColor
				 (
					sf::Color
					 (
						0,0,0,
						std::min(255.,a)
					 )
				 );

				Game::getWindow()->draw(s);

				if (a >= 255.)
				 {
					Game::setColourClear(sf::Color::Black);
					Game::getTimer()->resetTimeGlobal();
					++ state;
				 }
			 }
*/
		 }

		if (state < 16)
			Game::getWindow()->draw(textSpace);
	 }

	bool LevelFirst::isDone() const
	 {
		return (state == 16);
	 }

	void LevelFirst::onStart()
	 {
		Game::setColourClear(sf::Color::Black);
		Game::getTimer()->resetTimeGlobal();
	 }
 }
