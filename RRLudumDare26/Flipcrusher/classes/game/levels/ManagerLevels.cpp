//! @author Adam Emil Skoog, Royal Railway HB
//! @date   2013-04-28

// Updates.
// 2013-04-30

#include "ManagerLevels.h"
#include "../../assets/Image.h"
#include "../Object.h"
#include "../Game.h"
#include "Level.h"
#include "LevelFirst.h"
#include "LevelRoll.h"
#include "LevelHoles.h"
#include "LevelOutro.h"
#include "../../../ResourcePath.h"
#include <sstream>

#ifdef DEBUG
	#include <iostream>
#endif

namespace flip
 {
	ManagerLevels::ManagerLevels()
	:
	countCleared(0),
	state(STATE_LEVEL),
	substate(0),
	level(LEVEL_0)
	 {
		levelCurrent = new LevelFirst;

		for (unsigned i(0); i < 2; ++ i)
		 {
			for (unsigned j(0); j < COUNT_LEVELS - 2; ++ j)
			 {
				std::stringstream stream;
				stream << "graphics/iconStage" <<
				          ((i == 1) ? "Done" : "") << j << ".png";
				icons[i][j] = new Image(stream.str());
				icons[i][j]->setOrigin
				 (
					icons[i][j]->getLocalBounds().width / 2,
					icons[i][j]->getLocalBounds().height / 2
				 );
				icons[i][j]->setPosition
				 (
					Game::getWindow()->getSize().x * (j + 1) +
					icons[i][j]->getLocalBounds().width / 2,
					Game::getWindow()->getSize().y / 2
				 );
			 }
		 }

		music = new sf::Music;

		if (!music->openFromFile(sf::resourcePath() + "sounds/musicSuspence.ogg"))
		 {
			#ifdef DEBUG
				std::cout << "Failed to load sound 'sounds/musicSuspence.ogg'. Aborting execution." << std::endl;
			#endif

			delete music;
			exit(EXIT_FAILURE);
		 }

		music->setLoop(true);
	 }

	void ManagerLevels::updateAndDraw()
	 {
		switch (state)
		 {
			case STATE_BETWEEN:
			 {
				if (level != COUNT_LEVELS)
				 {
					for (unsigned i(0); i < COUNT_LEVELS - 2; ++ i)
					 {
						if (substate == 0 && (Game::getTimer()->getTimeGlobal() > 1. || countCleared == 0))
						 {
							const float d(Game::getWindow()->getSize().x / 2);

							if (icons[0][countCleared]->getPosition().x > d)
							 {
								for (unsigned j(0); j < 2; ++ j)
									icons[j][i]->move(Game::getTimer()->getTimeGlobalDelta() * -350.f,.0f);

								if (icons[0][countCleared]->getPosition().x <= d)
								 {
									const float distance(icons[0][countCleared]->getPosition().x - d);

									for (unsigned j(0); j < 2; ++ j)
										for (unsigned k(0); k < COUNT_LEVELS - 2; ++ k)
											icons[j][k]->move(distance,.0f);

									++ substate;
									Game::getTimer()->resetTimeGlobal();
									break;
								 }
							 }
						 }

						Game::getWindow()->draw(*icons[(countCleared > i)][i]);

						switch (substate)
						 {
							case 1:
							 {
								if (Game::getTimer()->getTimeGlobal() > .8)
								 {
									music->stop();
									Object::soundFlip->play();
									Game::getTimer()->resetTimeGlobal();
									Game::setColourClear(sf::Color::White);
									++ countCleared;
									++ substate;
								 }

								break;
							 }

							case 2:
							 {
								if (Game::getTimer()->getTimeGlobal() > .5)
								 {
									Game::getTimer()->resetTimeGlobal();
									++ substate;
								 }

								break;
							 }
						
							case 3:
							 {
								const double a(255. * Game::getTimer()->getTimeGlobal());
								sf::RectangleShape s(sf::Vector2f(Game::getWindow()->getSize().x,Game::getWindow()->getSize().y));
								s.setFillColor
								 (
									sf::Color
									 (
										255,255,255,
										std::min(255.,(a / 25.) * 25.)
									 )
								 );
								Game::getWindow()->draw(s);

								if (a >= 255.)
								 {
									Game::getTimer()->resetTimeGlobal();
									Game::getTimer()->resetTimeGame();
									Game::setColourClear(sf::Color::White);
									substate = 0;
									state = STATE_LEVEL;

									if (levelCurrent)
										levelCurrent->start();
								 }

								break;
							 }
						 }
					 }
				 }

				break;
			 }

			case STATE_LEVEL:
			 {
				if (levelCurrent)
				 {
					levelCurrent->updateAndDraw();

					if (levelCurrent->isDone())
					 {
						Game::setColourClear(sf::Color::Black);
						Game::getTimer()->resetTimeGlobal();
						Game::getTimer()->resetTimeGame();
						delete levelCurrent;
						levelCurrent = NULL;
						++ level;

						if (level < OUTRO)
						 {
							state = STATE_BETWEEN;
							music->play();
						 }

						Game::getWindow()->setView(Game::getWindow()->getDefaultView());

						if (level == LEVEL_1)
							levelCurrent = new LevelRoll;
						else if (level == LEVEL_2)
							levelCurrent = new LevelHoles;
						else if (level == OUTRO)
						 {
							levelCurrent = new LevelOutro;
							levelCurrent->start();
						 }

						Object::setFlipEnabled(false);
					 }
				 }

				break;
			 }
		 }
	 }

	void ManagerLevels::start()
	 {
		Game::setColourClear(sf::Color::Black);
		levelCurrent->start();
		music->play();
	 }

	ManagerLevels::~ManagerLevels()
	 {
		delete music;

		for (unsigned i(0); i < 2; ++ i)
			for (unsigned j(0); j < COUNT_LEVELS - 2; ++ j)
				delete icons[i][j];

		if (levelCurrent)
			delete levelCurrent;
	 }

	sf::Music *ManagerLevels::music = NULL;
 }
