//! @author Adam Emil Skoog, Royal Railway HB
//! @date   2013-04-28

// Updates:
// 2013-04-29

#ifndef __RRLudumDare26__LevelRoll__
	#define __RRLudumDare26__LevelRoll__
	#include "Level.h"
    #include "../Object.h"
	#include "../../assets/Image.h"
	#include <SFML/Audio.hpp>
	#include <vector>

    namespace flip
     {
		class Sound;

		class LevelRoll : public Level
		 {
			public:
				class Object2 : public Object
				 {
					public:
						explicit Object2(const Colour & = Object::COLOUR_NEUTRAL);
						Colour colourOriginal;
				 };

				class Line : public Object2
				 {
					public:
						explicit Line(const sf::Vector2f & = sf::Vector2f(.0f,.0f),
									  const sf::Vector2f & = sf::Vector2f(.0f,.0f),
									  const Colour & = Object::COLOUR_NEUTRAL);
						void update();
						void draw() const;
						~Line();

						sf::Vector2f start, end;
						bool right;
						sf::Shape *shape;
						float oppositeAngle, a, unrestrictedAngle, angle;
				 };

				class Spike : public Object2
				 {
					public:
						explicit Spike(const float & = 0,const float & = 0,
						               const Colour & = Object::COLOUR_NEUTRAL);
						void update();
						void draw() const;
						Image shape;
						const sf::FloatRect getGlobalBounds() const;
				 };

				class Coin : public Object
				 {
					public:
						explicit Coin(const float & = 0,const float & = 0);
						void update();
						void draw() const;
						void hit();
						Image shape;
						bool wasHit;
						bool last;
				 };

				class LightController : public Object
				 {
					public:
						explicit LightController();

					protected:
						void onBlack();
						void onWhite();
						void dropCharacter();
				 };

				class Goal : public Object
				 {
					public:
						explicit Goal(const sf::Vector2f &);
						void update();
						void draw() const;
						const sf::FloatRect getGlobalBounds() const;

					private:
						mutable Image image, fire;
				 };

				class Character : public Object
				 {
					public:
						explicit Character();
						void update0();
						void update();
						void draw() const;
						void fall();
						void fallTrue(const bool &direct = false);
						const sf::Vector2f pointLine(const Line &) const;
						const sf::Vector2f fromLine(const Line &) const;
						void hit();

						mutable Image shape;
						Line *line;
						bool inAir;
						unsigned falling;
						sf::Vector2f speed;
						sf::View view;
						bool hitFirst, played;
				 };

				explicit LevelRoll();
				static const unsigned &getSizeGrid();
				void reset();
				bool isDone() const;
				~LevelRoll();

				static Object::Colour *colourWorld;

			protected:
				void update();
				void draw() const;
				void onStart();

			private:
                static Character *character;
				static Goal *goal;
				static unsigned *gridSize;
				std::vector<Line *> lines;
				std::vector<Spike *> spikes;
				std::vector<Coin *> coins;
				static Image *imageSpike, *imageCoin;
				static LevelRoll *instance;
				sf::Music music1;
				Sound *soundCoin, *soundHit, *soundWin;
				bool win;
				bool gotAll;
		 };
     }

#endif
