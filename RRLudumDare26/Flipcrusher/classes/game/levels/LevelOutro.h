//! @author Adam Emil Skoog, Royal Railway HB
//! @date   2013-04-30

#ifndef __Flipcrusher__LevelOutro__
	#define __Flipcrusher__LevelOutro__
	#include "Level.h"
	#include "../../assets/Image.h"
	#include <SFML/Audio.hpp>

	namespace flip
	 {
		class LevelOutro : public Level
		 {
			public:
				explicit LevelOutro();
				~LevelOutro();

			protected:
				void onStart();
				void update();
				void draw() const;

				unsigned state;

			private:
				enum CountStars
				 {
					COUNT_STARS = 60
				 };

				Image imageMoon, imageCharacter, *imageEnemy[2],
					  imageCharacter2, credits;
				sf::RectangleShape stars[COUNT_STARS];
				sf::Music music0, music1;

				void resetStars();
		 };
	 }
#endif
