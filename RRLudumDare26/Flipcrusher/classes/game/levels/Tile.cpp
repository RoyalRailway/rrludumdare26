//! @author Adam Emil Skoog, Royal Railway HB
//! @date   2013-04-27

#include "Tile.h"
#include "../Game.h"
#include "../../assets/Image.h"

namespace flip
 {
	Tile::Tile()
	:
	shape(sf::Vector2f(Game::getSizeGrid(),Game::getSizeGrid())),
	goal(false)
	 {
		for (unsigned i(0); i < COUNT_CONNECTIONS; ++ i)
			connections[i] = false;
	 }

	void Tile::initialise()
	 {
		imageTile    = new Image("graphics/tileLevelHoles.png");
		imageNeutral = new Image("graphics/neutralLevelHoles.png");
		imageGoal    = new Image("graphics/tileGoal.png");
	 }

	void Tile::deinitialise()
	 {
		delete imageGoal;
		delete imageNeutral;
		delete imageTile;
	 }

	void Tile::update()
	 {
		shape.setPosition(position);
	 }

	void Tile::draw() const
	 {
		Image *const i((goal) ? imageGoal : ((getColour() == Object::COLOUR_NEUTRAL) ? imageNeutral : imageTile));
		imageTile->setColor((getColour() == COLOUR_BLACK) ? sf::Color::Black : sf::Color::White);
		i->setPosition(shape.getPosition());
		Game::getWindow()->draw(*i);

		if (getColour() != Object::COLOUR_NEUTRAL)
		 {
			const sf::Vector2f p(shape.getPosition());

			if (connections[LEFT])
			 {
				shape.move(shape.getLocalBounds().width / -2.f,0);
				Game::getWindow()->draw(shape);
				shape.setPosition(p);
			 }

			if (connections[RIGHT])
			 {
				shape.move(shape.getLocalBounds().width / 2.f,0);
				Game::getWindow()->draw(shape);
				shape.setPosition(p);
			 }

			if (connections[UP])
			 {
				shape.move(0,shape.getLocalBounds().height / -2.f);
				Game::getWindow()->draw(shape);
				shape.setPosition(p);
			 }

			if (connections[DOWN])
			 {
				shape.move(0,shape.getLocalBounds().height / 2);
				Game::getWindow()->draw(shape);
				shape.setPosition(p);
			 }
		 }
	 }

	const sf::FloatRect Tile::getGlobalBounds() const
	 {
		return shape.getGlobalBounds();
	 }

	Tile &Tile::setConnection(const Connection &connection,const bool &set)
	 {
		connections[connection] = set;
		return *this;
	 }

	const bool &Tile::getConnection(const Connection &connection) const
	 {
		return connections[connection];
	 }

	void Tile::onWhite()
	 {
		shape.setFillColor(sf::Color::White);
	 }

	void Tile::onBlack()
	 {
		shape.setFillColor(sf::Color::Black);
	 }

	Image *Tile::imageTile, *Tile::imageNeutral, *Tile::imageGoal;
 }
