//! @author Adam Emil Skoog, Royal Railway HB
//! @date   2013-04-27

#ifndef __RRLudumDare26__Tile__
	#define __RRLudumDare26__Tile__
	#include "../Object.h"

	namespace flip
	 {
		class Image;

		class Tile : public Object
		 {
			public:
				enum Connection
				 {
					LEFT,
					RIGHT,
					UP,
					DOWN,
					COUNT_CONNECTIONS
				 };

				explicit Tile();
				static void initialise();
				static void deinitialise();
				void update();
				void draw() const;
				const sf::FloatRect getGlobalBounds() const;
				Tile &setConnection(const Connection &,const bool &);
				const bool &getConnection(const Connection &) const;
				bool goal;

			protected:
				void onWhite();
				void onBlack();

			protected:
				static Image *imageTile, *imageNeutral, *imageGoal;

				mutable sf::RectangleShape shape;
				bool connections[COUNT_CONNECTIONS];
		 };
	 }
#endif
