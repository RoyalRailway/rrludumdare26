//! @author Adam Emil Skoog, Royal Railway HB
//! @date   2013-04-27

// Updates:
// 2013-04-28
// 2013-04-29

#include "LevelHoles.h"
#include "../Object.h"
#include "Tile.h"
#include "../../../ResourcePath.h"
#include "../../assets/Sound.h"
#include "../../../maths.h"

#ifdef DEBUG
	#include <iostream>
#endif

namespace flip
 {
	LevelHoles::LevelHoles()
	:
	rows(Game::getRows() + 4),
	columns(Game::getColumns()),
	blackDistribution(50),
	character(NULL),
	speedLevel(40.f),
	imageVignette(new Image("graphics/vignette.png")),
	soundHit(new Sound("sounds/hit.ogg")),
	soundScream(new Sound("sounds/scream.ogg")),
	soundWarning(new Sound("sounds/warningGame.ogg")),
	soundWin(new Sound("sounds/fanfare.ogg")),
	countFinish(0),
	win(false)
	 {
		soundWarning->setVolume(70.f);
		soundScream->setVolume(80.f);
		imageVignette->setPosition(0,0);

		Enemy::image = new Image("graphics/enemyHoles.png");

		if (!music.openFromFile(sf::resourcePath() + "sounds/musicLevel0.ogg"))
		 {
			#ifdef DEBUG
				std::cout << "Failed to load sound 'sounds/musicLevel0.ogg'. Aborting execution." << std::endl;
			#endif
			
			exit(EXIT_FAILURE);
		 }

		music.setLoop(true);

		Tile::initialise();

		objects = new Object **[rows];

		for (int i(0); i < rows; ++ i)
		 {
			objects[i] = new Object *[columns];

			for (int j(0); j < columns; ++ j)
				objects[i][j] = new Tile;
		 }

		for (unsigned i(0); i < 4; ++ i)
		 {
			enemies[i] = new Enemy;
			enemies[i]->position.x = Game::getSizeGrid() * ((i == 0 || i == 2) ? 2 : Game::getColumns() - 3);
			enemies[i]->position.y = Game::getSizeGrid() * ((i < 2) ? 2 : Game::getRows() - 3);
			enemies[i]->positionStart = enemies[i]->position;
			enemies[i]->level = this;

			const float speed = speedLevel / 2.f;

			switch (i)
			 {
				case 0:
				 {
					enemies[i]->speed.x = speed;
					enemies[i]->speed.y = speed;
					break;
				 }

				case 1:
				 {
					enemies[i]->speed.x = -speed;
					enemies[i]->speed.y = speed;
					break;
				 }

				case 2:
				 {
					enemies[i]->speed.x = speed;
					enemies[i]->speed.y = -speed;
					break;
				 }

				case 3:
				 {
					enemies[i]->speed.x = -speed;
					enemies[i]->speed.y = -speed;
					break;
				 }
			 }

			enemies[i]->bake();
		 }

		character = new Character;
		character->level = this;

		reset();
	 }

	bool LevelHoles::isDone() const
	 {
		return (win && soundWin->getStatus() != sf::Sound::Playing);
	 }

	void LevelHoles::onStart()
	 {
		music.play();
		Game::setColourClear(sf::Color(128,128,128));
		Object::setFlipEnabled();
	 }

	void LevelHoles::update()
	 {
		speedLevel += Game::getTimer()->getTimeGameDelta() / 2.f;

		if (speedLevel > 70.f)
			speedLevel = 70.f;

		const float speed(speedLevel);

		for (unsigned i(0); i < 4; ++ i)
			enemies[i]->hitWhite =
			enemies[i]->hitBlack = false;

		character->hitBlack =
		character->hitWhite = false;

		bool movedTop(false);

		for (unsigned i(0); i < rows; ++ i)
		 {
			for (unsigned j(0); j < columns; ++ j)
			 {
				objects[i][j]->position.y -= speed * Game::getTimer()->getTimeGameDelta();

				if (objects[i][j]->position.y <= Game::getSizeGrid() * ((rows - Game::getRows()) / -2.f))
					movedTop = true;

				for (unsigned k(0); k < 4; ++ k)
				 {
					if (reinterpret_cast<Tile *>(objects[i][j])->getGlobalBounds().intersects(enemies[k]->getGlobalBounds(objects[i][j]->getColour())))
					 {
						if (objects[i][j]->getColour() == Object::COLOUR_BLACK)
							enemies[k]->hitBlack = true;
						else
							enemies[k]->hitWhite = true;
					 }
				 }

				if (reinterpret_cast<Tile *>(objects[i][j])->getGlobalBounds().intersects(character->getGlobalBounds(objects[i][j]->getColour())))
				 {
					if (objects[i][j]->getColour() == Object::COLOUR_BLACK)
						character->hitBlack = true;
					else
					 {
						character->hitWhite = true;

						if (!win && reinterpret_cast<Tile *>(objects[i][j])->goal)
						 {
							win = true;
							Object::setFlipEnabled(false);
							music.stop();
							soundWin->play();

							for (unsigned k(0); k < 4; ++ k)
								enemies[k]->hit = true;

							soundHit->play();
						 }
					 }
				 }
			 }
		 }

		if (movedTop)
		 {
			const double limitTime(60.);

			if (Game::getTimer()->getTimeGame() >= limitTime)
			 {
				++ countFinish;
				soundWarning->stop();
			 }

			Object *temporary[columns];

			for (unsigned i(0); i < columns; ++ i)
				temporary[i] = objects[0][i];

			for (unsigned i(0); i < rows - 1; ++ i)
				for (unsigned j(0); j < columns; ++ j)
					objects[i][j] = objects[i + 1][j];

			for (unsigned i(0); i < columns; ++ i)
			 {
				objects[rows - 1][i] = temporary[i];
				objects[rows - 1][i]->position.y += Game::getSizeGrid() * (rows - 1);// (rows + ((rows - Game::getRows()) / -2.f));//(rows + ((rows - Game::getRows()) / -2.f));

				if (Game::getTimer()->getTimeGame() < limitTime)
					objects[rows - 1][i]->setColour((std::rand() % 100 < blackDistribution * 1.1f) ? Object::COLOUR_BLACK : Object::COLOUR_WHITE);
				else
				 {
					objects[rows - 1][i]->setColour(Object::COLOUR_NEUTRAL);

					if (countFinish == 7)
						reinterpret_cast<Tile *>(objects[rows - 1][i])->goal = true;
					else
						reinterpret_cast<Tile *>(objects[rows - 1][i])->goal = false;
				 }
			 }

			bake();
		 }

		for (unsigned i(0); i < 4; ++ i)
		 {
			if (enemies[i]->hitBlack && !enemies[i]->hitWhite)
			 {
				enemies[i]->hit = true;
				enemies[i]->warned = false;

				if (enemies[i] == Enemy::warner)
				 {
					soundWarning->stop();
					Enemy::warner = NULL;
				 }
			 }
			else if (!win && !enemies[i]->hit && !character->hit && enemies[i]->getGlobalBounds(Object::COLOUR_BLACK).intersects(character->getGlobalBounds(Object::COLOUR_BLACK)))
			 {
				if (enemies[i] == Enemy::warner)
					Enemy::warner = NULL;

				soundWarning->stop();
				enemies[i]->hit = true;
				enemies[i]->warned = false;
				character->hit = true;
				Object::setFlipEnabled(false);
				soundHit->play();
			 }
		 }

		if (!win && !character->hit && character->hitBlack && !character->hitWhite)
		 {
			soundWarning->stop();
			character->hit = true;
			Object::setFlipEnabled(false);
			soundScream->play();
		 }
	 }

	void LevelHoles::draw() const
	 {
	 /*
			std::stringstream ss;
			ss << std::floor(speedLevel);
			sf::Text t(ss.str(),*Game::getFont(),20);
			t.setColor(sf::Color::Red);
			t.setPosition(40,40);
			Game::getWindow()->draw(t);
			*/
	 /*
		for (int i(0); i < rows; ++ i)
		 {
			std::stringstream ss;
			ss << i;
			sf::Text t(ss.str(),*Game::getFont(),11);
			t.setColor(sf::Color::Red);
			t.setPosition(40,objects[i][0]->position.y);
			Game::getWindow()->draw(t);
		 }*/
/*		for(unsigned i(0); i<4;++i)
		{
			sf::RectangleShape sa(sf::Vector2f(enemies[i]->getGlobalBounds().width,enemies[i]->getGlobalBounds().height));
			sa.setPosition(enemies[i]->getGlobalBounds().left,enemies[i]->getGlobalBounds().top);
			sa.setFillColor(sf::Color::Red);
			Game::getWindow()->draw(sa);
		}
*//*
		{
		sf::RectangleShape sa(sf::Vector2f(character->getGlobalBounds(Object::COLOUR_BLACK).width,character->getGlobalBounds(Object::COLOUR_BLACK).height));
		sa.setPosition(character->getGlobalBounds(Object::COLOUR_BLACK).left,character->getGlobalBounds(Object::COLOUR_BLACK).top);
		sa.setFillColor(sf::Color::Green);
		Game::getWindow()->draw(sa);
		}

		{
		sf::RectangleShape sa(sf::Vector2f(character->getGlobalBounds(Object::COLOUR_WHITE).width,character->getGlobalBounds(Object::COLOUR_WHITE).height));
		sa.setPosition(character->getGlobalBounds(Object::COLOUR_WHITE).left,character->getGlobalBounds(Object::COLOUR_WHITE).top);
		sa.setFillColor(sf::Color::Blue);
		Game::getWindow()->draw(sa);
		}
	*/
		Game::getWindow()->draw(*imageVignette);

		if (soundWarning->getStatus() == sf::Sound::Playing)
		 {
			sf::RectangleShape s(sf::Vector2f(Game::getWindow()->getSize().x,Game::getWindow()->getSize().y));
			s.setPosition(.0f,.0f);
			s.setFillColor(sf::Color(255,0,0,60.f + 40.f * std::sin(Game::getTimer()->getTimeGame() * 9.7)));
			Game::getWindow()->draw(s);
		 }
	 }

	void LevelHoles::bake()
	 {
		for (int i(0); i < rows; ++ i)
		 {
			for (int j(0); j < columns; ++ j)
			 {
				if (objects[i][j]->getColour() == Object::COLOUR_BLACK)
				 {
					unsigned x[2], y[2];
					x[0] = (j > 0)           ? j - 1 : columns - 1;
					y[0] = (i > 0)           ? i - 1 : rows - 1;
					x[1] = (j < columns - 1) ? j + 1 : 0;
					y[1] = (i < rows - 1)    ? i + 1 : 0;

					for (unsigned k(0); k < 2; ++ k)
						if (objects[i][x[k]]->getColour() == Object::COLOUR_WHITE ||
							x[k] == 0 || x[k] == columns - 1)
							objects[i][x[k]]->setColour(Object::COLOUR_NEUTRAL);

					for (unsigned k(0); k < 2; ++ k)
						if (objects[y[k]][j]->getColour() == Object::COLOUR_WHITE ||
							y[k] == 0 || y[k] == rows - 1)
							objects[y[k]][j]->setColour(Object::COLOUR_NEUTRAL);
				 }
			 }
		 }

		for (int i(0); i < rows; ++ i)
		 {
			for (int j(0); j < columns; ++ j)
			 {
				if (objects[i][j]->getColour() != Object::COLOUR_NEUTRAL)
				 {
					unsigned x[2], y[2];
					x[0] = (j > 0)           ? j - 1 : columns - 1;
					y[0] = (i > 0)           ? i - 1 : rows - 1;
					x[1] = (j < columns - 1) ? j + 1 : 0;
					y[1] = (i < rows - 1)    ? i + 1 : 0;

					const Object::Colour &c(objects[i][j]->getColour());
					Tile *const t(reinterpret_cast<Tile *>(objects[i][j]));

					t->setConnection(Tile::LEFT,(j == 0 || objects[i][x[0]]->getColour() == c));
					t->setConnection(Tile::RIGHT,(j == columns - 1 || objects[i][x[1]]->getColour() == c));
					t->setConnection(Tile::UP,(objects[y[0]][j]->getColour() == c));
					t->setConnection(Tile::DOWN,(objects[y[1]][j]->getColour() == c));
				 }
			 }
		 }
	 }

	void LevelHoles::reset()
	 {
		for (int i(0); i < rows; ++ i)
		 {
			for (int j(0); j < columns; ++ j)
			 {
				Tile *const tile(reinterpret_cast<Tile *>(objects[i][j]));

				tile->setColour(Object::COLOUR_WHITE);

				if (std::rand() % 100 < blackDistribution)
					tile->setColour(Object::COLOUR_BLACK);

				if (j >= std::floor(columns / 2.f) - 4 &&
				    j <= std::ceil(columns / 2.f) + 3 &&
					i >= std::floor(rows / 2.f) - 4 &&
					i <= std::ceil(rows / 2.f) + 3)
					tile->setColour(Object::COLOUR_WHITE);

				if ((j < 6 || j > columns - 7) &&
				    (i < 6 || i > rows - 7))
					tile->setColour(Object::COLOUR_WHITE);

				tile->position.x = Game::getSizeGrid() * (j - (columns - Game::getColumns()) / 2);
				tile->position.y = Game::getSizeGrid() * (i - 1);
				tile->goal = false;
			 }
		 }

		bake();

		if (character)
			character->reset();

		for (unsigned i(0); i < 4; ++ i)
			enemies[i]->reset();

		Game::getTimer()->resetTimeGame();
	 }

	LevelHoles::~LevelHoles()
	 {
		delete Enemy::image;
		Enemy::image = NULL;

		delete soundWin;
		delete soundWarning;
		delete soundScream;
		delete soundHit;
		delete imageVignette;

		for (unsigned i(0); i < rows; ++ i)
			delete[] objects[i];

		delete[] objects;

		Object::clearObjects();
		Tile::deinitialise();
	 }

	LevelHoles::Character::Character()
	:
	shape("graphics/character16.png"),
	hitBlack(false),
	hitWhite(false),
	hit(false)
	 {
//		std::floor(Game::getWindow()->getSize().x / 2. / Game::getSizeGrid()) * Game::getSizeGrid(),
		reset();
	 }

	void LevelHoles::Character::update()
	 {
		shape.setPosition
		 (
			std::floor(Game::getWindow()->getSize().y / 2.f / Game::getSizeGrid()) * Game::getSizeGrid() +
			Game::getSizeGrid() * 4.f * std::sin((Game::getTimer()->getTimeGlobal() / 4.f) * (level->speedLevel / 40.f)),
			shape.getPosition().y
		 );

		if (hit)
			shape.move(0,level->speedLevel * -.35f * Game::getTimer()->getTimeGameDelta());

		position = shape.getPosition();
/*
		if (Game::isInputActive())
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
				level->reset();
*/
		if (hit)
		 {
			const float ss(Game::getTimer()->getTimeGlobalDelta() * 4.);
			scale += ss;
			const float s(std::max(.0f,shape.getScale().x - ss));
			shape.setScale(s,s);

			if (scale >= 3.f)
				level->reset();
		 }
	 }

	void LevelHoles::Character::draw() const
	 {
		Game::getWindow()->draw(shape);
	 }

	const sf::FloatRect LevelHoles::Character::getGlobalBounds(const Object::Colour &colour) const
	 {
		const float add((colour == Object::COLOUR_BLACK) ? .17f : .3f);

		sf::FloatRect result(shape.getGlobalBounds());
		result.left   += Game::getSizeGrid() * add;
		result.top    += Game::getSizeGrid() * add;
		result.width  -= Game::getSizeGrid() * add * 2.f;
		result.height -= Game::getSizeGrid() * add * 2.f;
		return result;
	 }

	void LevelHoles::Character::reset()
	 {
		scale = .0f;
		hit = false;
		shape.setPosition(std::floor(Game::getWindow()->getSize().x / 2.f / Game::getSizeGrid()) * Game::getSizeGrid(),std::floor(Game::getWindow()->getSize().y / 2. / Game::getSizeGrid()) * Game::getSizeGrid());
		position = shape.getPosition();
		shape.setScale(1,1);
		Object::setFlipEnabled();
		Game::getTimer()->resetTimeGlobal();
	 }

	LevelHoles::Enemy::Enemy()
	:
	shape   (*image),
	speed   (.0f,.0f),
	hitBlack(false),
	hitWhite(false),
	hit     (false),
	warned  (false)
	 {
		shape.setOrigin(shape.getLocalBounds().width / 2,shape.getLocalBounds().height / 2);
	 }

	void LevelHoles::Enemy::update()
	 {
		if (hit)
		 {
			const float s(shape.getScale().x - Game::getTimer()->getTimeGlobalDelta() * 4.);

			if (s > .0f)
				shape.setScale(s,s);
			else if (!level->win)
			 {
				hit = false;
				shape.setScale(1,1);
				bake();
			 }
		 }

		if (!hit)
		 {
			shape.move
			 (
				speed.x * Game::getTimer()->getTimeGameDelta(),
				speed.y * Game::getTimer()->getTimeGameDelta()
			 );
		 }
		else;
			//shape.move(0,-14.f * Game::getTimer()->getTimeGameDelta());

		if (!level->win && !warned && !hit && level->countFinish == 0 &&
		    level->soundWarning->getStatus() != sf::Sound::Playing &&
			pointDistance(shape.getPosition(),level->character->shape.getPosition()) < Game::getWindow()->getSize().x / 5.f)
		 {
			warned = true;
			level->soundWarning->play();
			warner = this;
		 }
	 }

	void LevelHoles::Enemy::draw() const
	 {
		Game::getWindow()->draw(shape);
	 }

	void LevelHoles::Enemy::bake()
	 {
		shape.setPosition(position);
	 }

	void LevelHoles::Enemy::reset()
	 {
		position = positionStart;
		shape.setPosition(positionStart);
	 }

	const sf::FloatRect LevelHoles::Enemy::getGlobalBounds(const Object::Colour &colour) const
	 {
		const float add((colour == Object::COLOUR_BLACK) ? .1f : .3f);

		sf::FloatRect result(shape.getGlobalBounds());
		result.left   += Game::getSizeGrid() * add;
		result.top    += Game::getSizeGrid() * add;
		result.width  -= Game::getSizeGrid() * add * 2.f;
		result.height -= Game::getSizeGrid() * add * 2.f;
		return result;
	 }

	LevelHoles::Enemy *LevelHoles::Enemy::warner = NULL;
	Image *LevelHoles::Enemy::image = NULL;
 }
