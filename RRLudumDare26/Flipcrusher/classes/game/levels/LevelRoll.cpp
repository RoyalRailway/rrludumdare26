//! @author Adam Emil Skoog, Royal Railway HB
//! @date   2013-04-28

// Updates:
// 2013-04-29

#include "LevelRoll.h"
#include "../Game.h"
#include "../../../maths.h"
#include "../../../ResourcePath.h"
#include "../../assets/Sound.h"

#ifdef DEBUG
	#include <iostream>
#endif

namespace flip
 {
    LevelRoll::LevelRoll()
	:
	soundCoin(new Sound("sounds/coin.ogg")),
	soundHit(new Sound("sounds/hit.ogg")),
	soundWin(new Sound("sounds/fanfare.ogg")),
	win(false),
	gotAll(false)
     {
		if (!music1.openFromFile(sf::resourcePath() + "sounds/musicRoll.ogg"))
		 {
			#ifdef DEBUG
				std::cout << "Failed to load sound 'sounds/musicRoll.ogg'. Aborting execution." << std::endl;
			#endif
			
			exit(EXIT_FAILURE);
		 }

		music1.setLoop(true);

		imageSpike = new Image("graphics/spike.png");
		imageCoin = new Image("graphics/coin.png");
		instance = this;

		///// 1

		lines.push_back(new Line
		 (
			sf::Vector2f(0,getSizeGrid() * 3),
			sf::Vector2f(getSizeGrid() * 6,getSizeGrid() * 5)
		 ));

		lines.push_back(new Line
		 (
			sf::Vector2f(getSizeGrid() * 6,getSizeGrid() * 5),
			sf::Vector2f(getSizeGrid() * 12,getSizeGrid() * 7),
			Object::COLOUR_WHITE
		 ));

		lines.push_back(new Line
		 (
			sf::Vector2f(getSizeGrid() * 12,getSizeGrid() * 7),
			sf::Vector2f(getSizeGrid() * 21,getSizeGrid() * 10)
		 ));

		coins.push_back(new Coin(getSizeGrid() * 10.2f,getSizeGrid() * 9.5f));

		///// 2

		lines.push_back(new Line
		 (
			sf::Vector2f(0,getSizeGrid() * 9),
			sf::Vector2f(getSizeGrid() * 8,getSizeGrid() * 11)
		 ));

		lines.push_back(new Line
		 (
			sf::Vector2f(getSizeGrid() * 8,getSizeGrid() * 11),
			sf::Vector2f(getSizeGrid() * 12,getSizeGrid() * 12),
			Object::COLOUR_BLACK
		 ));

		lines.push_back(new Line
		 (
			sf::Vector2f(getSizeGrid() * 12,getSizeGrid() * 12),
			sf::Vector2f(getSizeGrid() * 20,getSizeGrid() * 14)
		 ));

		///// 3

		coins.push_back(new Coin(getSizeGrid() * 18,getSizeGrid() * 15.3f));

		for (unsigned i(0); i < 4; ++ i)
			spikes.push_back(new Spike
			 (
				getSizeGrid() * (16 + i),
				getSizeGrid() * 15.8f - 4 * i,
				Object::COLOUR_WHITE
			 ));

		for (unsigned i(0); i < 4; ++ i)
			spikes.push_back(new Spike
			 (
				getSizeGrid() * (6 + i),
				getSizeGrid() * 17.8 - 4 * i,
				Object::COLOUR_BLACK
			 ));

		lines.push_back(new Line
		 (
			sf::Vector2f(getSizeGrid() * 20,getSizeGrid() * 16),
			sf::Vector2f(getSizeGrid() * 10,getSizeGrid() * 18)
		 ));
		
		lines.push_back(new Line
		 (
			sf::Vector2f(getSizeGrid() * 10,getSizeGrid() * 18),
			sf::Vector2f(getSizeGrid() * 5,getSizeGrid() * 19)
		 ));
		
		lines.push_back(new Line
		 (
			sf::Vector2f(getSizeGrid() * 5,getSizeGrid() * 19),
			sf::Vector2f(0,getSizeGrid() * 20),
			Object::COLOUR_WHITE
		 ));

		///// 4
		
		lines.push_back(new Line
		 (
			sf::Vector2f(getSizeGrid() * 0,getSizeGrid() * 23),
			sf::Vector2f(getSizeGrid() * 4,getSizeGrid() * 25),
			Object::COLOUR_WHITE
		 ));
		
		lines.push_back(new Line
		 (
			sf::Vector2f(getSizeGrid() * 4,getSizeGrid() * 25),
			sf::Vector2f(getSizeGrid() * 6,getSizeGrid() * 26)
		 ));
		
		lines.push_back(new Line
		 (
			sf::Vector2f(getSizeGrid() * 6,getSizeGrid() * 26),
			sf::Vector2f(getSizeGrid() * 10,getSizeGrid() * 28),
			Object::COLOUR_BLACK
		 ));
		
		lines.push_back(new Line
		 (
			sf::Vector2f(getSizeGrid() * 10,getSizeGrid() * 28),
			sf::Vector2f(getSizeGrid() * 12,getSizeGrid() * 29)
		 ));
		
		lines.push_back(new Line
		 (
			sf::Vector2f(getSizeGrid() * 12,getSizeGrid() * 29),
			sf::Vector2f(getSizeGrid() * 16,getSizeGrid() * 31),
			Object::COLOUR_WHITE
		 ));
		
		lines.push_back(new Line
		 (
			sf::Vector2f(getSizeGrid() * 16,getSizeGrid() * 31),
			sf::Vector2f(getSizeGrid() * 20,getSizeGrid() * 33),
			Object::COLOUR_BLACK
		 ));

		for (unsigned i(0); i < 4; ++ i)
			for (unsigned j(0); j < 3; ++ j)
				spikes.push_back(new Spike
				 (
					getSizeGrid() * (i + j * 6),
					getSizeGrid() * (26 + j * 4)
				 ));

		///// 5

		lines.push_back(new Line
		 (
			sf::Vector2f(getSizeGrid() * 20,getSizeGrid() * 36),
			sf::Vector2f(getSizeGrid() * 15,getSizeGrid() * 38)
		 ));

		lines.push_back(new Line
		 (
			sf::Vector2f(getSizeGrid() * 15,getSizeGrid() * 38),
			sf::Vector2f(getSizeGrid() * 10,getSizeGrid() * 40),
			Object::COLOUR_BLACK
		 ));

		lines.push_back(new Line
		 (
			sf::Vector2f(getSizeGrid() * 10,getSizeGrid() * 40),
			sf::Vector2f(getSizeGrid() * 5,getSizeGrid() * 42),
			Object::COLOUR_WHITE
		 ));

		lines.push_back(new Line
		 (
			sf::Vector2f(getSizeGrid() * 5,getSizeGrid() * 42),
			sf::Vector2f(0,getSizeGrid() * 44)
		 ));

		coins.push_back(new Coin(getSizeGrid() * 2,getSizeGrid() * 42));

		///// 6

		lines.push_back(new Line
		 (
			sf::Vector2f(getSizeGrid() * 21,getSizeGrid() * 42),
			sf::Vector2f(getSizeGrid() * 12,getSizeGrid() * 45)
		 ));

		lines.push_back(new Line
		 (
			sf::Vector2f(getSizeGrid() * 12,getSizeGrid() * 45),
			sf::Vector2f(getSizeGrid() * 3,getSizeGrid() * 48),
			Object::COLOUR_WHITE
		 ));

		lines.push_back(new Line
		 (
			sf::Vector2f(getSizeGrid() * 3,getSizeGrid() * 48),
			sf::Vector2f(0,getSizeGrid() * 49),
			Object::COLOUR_BLACK
		 ));

		for (unsigned i(0); i < 2; ++ i)
			spikes.push_back(new Spike
			 (
				getSizeGrid() * i,
				getSizeGrid() * 51
			 ));

		coins.push_back(new Coin(getSizeGrid() * 1.3f,getSizeGrid() * 48));

		///// 7

		lines.push_back(new Line
		 (
			sf::Vector2f(0,getSizeGrid() * 53),
			sf::Vector2f(getSizeGrid() * 5,getSizeGrid() * 54)
		 ));

		lines.push_back(new Line
		 (
			sf::Vector2f(getSizeGrid() * 5,getSizeGrid() * 54),
			sf::Vector2f(getSizeGrid() * 10,getSizeGrid() * 55),
			Object::COLOUR_BLACK
		 ));

		lines.push_back(new Line
		 (
			sf::Vector2f(getSizeGrid() * 10,getSizeGrid() * 55),
			sf::Vector2f(getSizeGrid() * 20,getSizeGrid() * 57)
		 ));

		///// 8
		for (unsigned i(0); i < 2; ++ i)
			spikes.push_back(new Spike
			 (
				getSizeGrid() * (11.5f + i),
				getSizeGrid() * 57.6f + 4 * i,
				Object::COLOUR_BLACK
			 ));

		for (unsigned i(0); i < 2; ++ i)
			spikes.push_back(new Spike
			 (
				getSizeGrid() * (18 + i),
				getSizeGrid() * 59 + 4 * i,
				Object::COLOUR_WHITE
			 ));

		lines.push_back(new Line
		 (
			sf::Vector2f(0,getSizeGrid() * 56),
			sf::Vector2f(getSizeGrid() * 5,getSizeGrid() * 57)
		 ));

		lines.push_back(new Line
		 (
			sf::Vector2f(getSizeGrid() * 5,getSizeGrid() * 57),
			sf::Vector2f(getSizeGrid() * 10,getSizeGrid() * 58),
			Object::COLOUR_BLACK
		 ));

		lines.push_back(new Line
		 (
			sf::Vector2f(getSizeGrid() * 10,getSizeGrid() * 58),
			sf::Vector2f(getSizeGrid() * 15,getSizeGrid() * 59)
		 ));

		lines.push_back(new Line
		 (
			sf::Vector2f(getSizeGrid() * 15,getSizeGrid() * 59),
			sf::Vector2f(getSizeGrid() * 20,getSizeGrid() * 60),
			Object::COLOUR_WHITE
		 ));

		///// 9

		for (unsigned i(0); i < 4; ++ i)
			spikes.push_back(new Spike
			 (
				getSizeGrid() * (16 + i),
				getSizeGrid() * 66,
				Object::COLOUR_BLACK
			 ));

		lines.push_back(new Line
		 (
			sf::Vector2f(getSizeGrid() * 20,getSizeGrid() * 64),
			sf::Vector2f(getSizeGrid() * 15,getSizeGrid() * 66),
			Object::COLOUR_WHITE
		 ));

		lines.push_back(new Line
		 (
			sf::Vector2f(getSizeGrid() * 15,getSizeGrid() * 66),
			sf::Vector2f(getSizeGrid() * 10,getSizeGrid() * 68),
			Object::COLOUR_BLACK
		 ));

		lines.push_back(new Line
		 (
			sf::Vector2f(getSizeGrid() * 10,getSizeGrid() * 68),
			sf::Vector2f(getSizeGrid() * 5,getSizeGrid() * 70),
			Object::COLOUR_WHITE
		 ));

		lines.push_back(new Line
		 (
			sf::Vector2f(getSizeGrid() * 5,getSizeGrid() * 70),
			sf::Vector2f(0,getSizeGrid() * 72)
		 ));

		Coin *lastCoin(new Coin(getSizeGrid() * 2,getSizeGrid() * 70));
		lastCoin->last = true;
		coins.push_back(lastCoin);

		///// 10

		for (unsigned i(0); i < 8; ++ i)
			if (i < 2 || i > 4)
				spikes.push_back(new Spike
				 (
					getSizeGrid() * (8 + i),
					getSizeGrid() * 74.6f + 6 * i,
					(i <= 1) ? Object::COLOUR_BLACK : Object::COLOUR_WHITE
				 ));

		lines.push_back(new Line
		 (
			sf::Vector2f(0,getSizeGrid() * 73),
			sf::Vector2f(getSizeGrid() * 17,getSizeGrid() * 78)
		 ));

		///// 11
		goal = new Goal(sf::Vector2f(0,getSizeGrid() * 88));

        character = new Character;

		new LightController;
     }

	void LevelRoll::reset()
	 {
		gotAll = false;

		if (*colourWorld != Object::COLOUR_BLACK)
			Object::flipColours(false);

		for (unsigned i(0); i < coins.size(); ++ i)
			coins[i]->wasHit = false;
	 }

	bool LevelRoll::isDone() const
	 {
		return (win && soundWin->getStatus() != sf::Sound::Playing);
	 }

	const unsigned &LevelRoll::getSizeGrid()
	 {
		return *gridSize;
	 }

    void LevelRoll::update()
     {
		if (!win)
		 {
			if (std::abs(goal->position.y - character->shape.getPosition().y) <
			    getSizeGrid() * 6)
			 {
				if (goal->getGlobalBounds().intersects(character->shape.getGlobalBounds()))
				 {
					if (gotAll)
					 {
						music1.stop();
						soundWin->play();
						Object::setFlipEnabled(false);
						win = true;
					 }
					else
						character->hit();
				 }
			 }
		 }

		character->update0();

		if (!win)
		 {
			bool hitLine(false);

			if (character->inAir)
			 {
				for (unsigned i(0); i < lines.size(); ++ i)
				 {
					const sf::Vector2f pl(character->pointLine(*lines[i]));

					// Check altitudes.
	//				if (lines[i]->end.y >= character->shape.getPosition().y && (lines[i]->start.y <= character->shape.getPosition().y ||
	//					std::abs(character->shape.getPosition().y - lines[i]->start.y) < character->shape.getLocalBounds().height))
					if (lines[i]->colourOriginal != *LevelRoll::colourWorld)
					 {
						// Check bounding boxes.
	//					if (lines[i]->shape->getGlobalBounds().intersects(character->shape.getGlobalBounds()))
						if (pl.y >= lines[i]->start.y && pl.y <= lines[i]->end.y &&
							pl.x >= std::min(lines[i]->start.x,lines[i]->end.x) &&
							pl.x <= std::max(lines[i]->start.x,lines[i]->end.x))
						 {
	/*						sf::Vector2f p[4];
							p[0].x = character->shape.getPosition().x;
							p[0].y = character->shape.getPosition().y - character->shape.getOrigin().y;
							p[1].x = character->shape.getPosition().x;
							p[1].y = character->shape.getPosition().y + character->shape.getOrigin().y;
							p[2] = lines[i]->start;
							p[3] = lines[i]->end;
	*/
	//						if (doLinesIntersect(p))
							const float H(lines[i]->end.y - lines[i]->start.y),
										B(std::abs(lines[i]->end.x - lines[i]->start.x)),
										s(H / B),
										y(lines[i]->start.y + s * std::abs(character->shape.getPosition().x - lines[i]->start.x));

							if (std::abs(pl.y - y) < lines[i]->unrestrictedAngle)
							 {
								character->inAir = false;
								hitLine = true;
								character->line = lines[i];

								if (!character->hitFirst || (character->falling == 0 && !character->line))
								 {
									character->speed.x = character->speed.y * std::abs(lines[i]->angle) / 20.f;
									character->hitFirst = true;
								 }

								character->speed.y = .0f;
								break;
							 }
						 }
					 }
				 }
			 }

			if (character->falling > 0)
				character->fallTrue(hitLine);

			for (unsigned i(0); i < spikes.size(); ++ i)
			 {
				if (spikes[i]->colourOriginal != *LevelRoll::colourWorld)
				 {
					if (std::abs(spikes[i]->shape.getPosition().y -
								 character->shape.getPosition().y) < getSizeGrid() * 2)
					 {
						if (spikes[i]->getGlobalBounds().intersects(character->shape.getGlobalBounds()))
						 {
							character->hit();
							break;
						 }
					 }
				 }
			 }

			for (unsigned i(0); i < coins.size(); ++ i)
			 {
				if (!coins[i]->wasHit)
				 {
					if (std::abs(coins[i]->shape.getPosition().y -
								 character->shape.getPosition().y) < getSizeGrid() * 2)
					 {
						if (coins[i]->shape.getGlobalBounds().intersects(character->shape.getGlobalBounds()))
						 {
							soundCoin->play();
							coins[i]->hit();

							if (coins[i]->last)
								gotAll = true;
						 }
					 }
				 }
			 }
		 }
     }

    void LevelRoll::draw() const
     {
     }

	void LevelRoll::onStart()
	 {
		music1.play();
		Game::setColourClear(sf::Color::Black);
		Game::getTimer()->resetTimeGame();
		Object::setFlipEnabled();
	 }

    LevelRoll::~LevelRoll()
     {
		delete soundWin;
		delete soundHit;
		delete soundCoin;
		delete imageCoin;
		delete imageSpike;
		delete gridSize;
		delete colourWorld;
		Object::clearObjects();
     }

	LevelRoll::Character::Character()
	:
	shape("graphics/character32.png"),
	line (NULL),
	inAir(true),
	falling(0),
	speed(.0f,40.f),
	view(Game::getWindow()->getDefaultView()),
	hitFirst(false),
	played(false)
	 {
		Game::getWindow()->setView(view);
		shape.setPosition(LevelRoll::getSizeGrid() * 2,.0f);
		shape.setOrigin(shape.getLocalBounds().width / 2,shape.getLocalBounds().height / 2);
	 }

	void LevelRoll::Character::update0()
	 {
		if (!LevelRoll::instance->win)
		 {
			if (shape.getPosition().y > Game::getWindow()->getSize().y / 2.f)
			 {
				view.setCenter(view.getCenter().x,shape.getPosition().y);
				Game::getWindow()->setView(view);
			 }

//			if (Game::isInputActive() && sf::Keyboard::isKeyPressed(sf::Keyboard::W))
//				shape.setPosition(getSizeGrid() * 17,getSizeGrid() * 83);
		 }

		if (inAir || LevelRoll::instance->win)
		 {
			if (!LevelRoll::instance->win)
				speed.y += Game::getTimer()->getTimeGameDelta() * 140.f;

			const float r(Game::getTimer()->getTimeGameDelta() * 3.f * ((speed.x > .0f) ? 1.f : -1.f));
			speed.x -= (std::abs(speed.x) > std::abs(r)) ? r : speed.x;
		 }

		shape.move
		 (
			speed.x * Game::getTimer()->getTimeGameDelta(),
			speed.y * Game::getTimer()->getTimeGameDelta() *
			((!LevelRoll::instance->win) ? 1.f : .0f)
		 );
	 }

	void LevelRoll::Character::update()
	 {
		shape.rotate(speed.x * Game::getTimer()->getTimeGameDelta() * 1.5f);

		if (!LevelRoll::instance->win)
		 {
			if (falling == 0 && line)
			 {
				const float x(pointLine(*line).x);

				if ((line->right && x >= line->start.x && x <= line->end.x) ||
					(!line->right && x <= line->start.x && x >= line->end.x))
				 {
					shape.setPosition(fromLine(*line));
					speed.x += Game::getTimer()->getTimeGameDelta() * line->angle * 5.f;
				 }
				else
					fall();
			 }

			if (shape.getPosition().x < shape.getOrigin().x ||
				shape.getPosition().x > Game::getWindow()->getSize().x - shape.getOrigin().x)
			 {
				speed.x *= -.52f;

				if (std::abs(speed.x) < 1.f && !inAir)
					hit();
				else
					shape.move(speed.x * 10.f * Game::getTimer()->getTimeGameDelta(),.0f);
			 }
		 }
		else
			shape.setPosition(shape.getPosition().x,LevelRoll::instance->goal->position.y - shape.getOrigin().y);
	 }

	void LevelRoll::Character::draw() const
	 {
		Game::getWindow()->draw(shape);
/*
		if (bajs || line)
		 {
			sf::CircleShape s(4);
			s.setFillColor((bajs)?sf::Color::Blue:sf::Color::Green);
			s.setPosition(pointLine((bajs)?*bajsLine:*line));
			s.setOrigin(s.getLocalBounds().width / 2,s.getLocalBounds().height / 2);
			Game::getWindow()->draw(s);
		 }*/
	 }

	void LevelRoll::Character::fall()
	 {
		if (falling == 0)
		 {
			played = false;
			falling = 1;
			inAir = true;
			speed.y = .0f;

			if (std::abs(speed.x) < 1.f)
			 {
				speed.x = ((speed.x > .0f) ? 10.f : -10.f);
				shape.move(3.f * ((shape.getPosition().x < Game::getWindow()->getSize().x / 2) ? 1.f : -1.f),.0f);
			 }
		 }
	 }

	void LevelRoll::Character::fallTrue(const bool &direct)
	 {
		if (!direct)
		 {
			falling = 0;
			shape.move(2.f * ((speed.x > .0f) ? 1.f : -1.f),.0f);
			speed.y = std::abs(speed.x) + 1.f;
			speed.x /= std::max(1.f,line->a * 1.5f);
			line = NULL;
			inAir = true;
		 }
	 }

	const sf::Vector2f LevelRoll::Character::pointLine(const Line &line) const
	 {
		return sf::Vector2f
		 (
			shape.getPosition().x + std::cos(line.oppositeAngle) * (shape.getOrigin().x - 1.f),
			shape.getPosition().y - std::sin(line.oppositeAngle) * (shape.getOrigin().y - 1.f)
		 );
	 }

	const sf::Vector2f LevelRoll::Character::fromLine(const Line &line) const
	 {
		const float H(line.end.y - line.start.y),
					B(std::abs(line.end.x - line.start.x)),
					s(H / B),
					x(pointLine(line).x),
					y(line.start.y + s * std::abs(x - line.start.x));

		return sf::Vector2f
		 (
			shape.getPosition().x,
			y - (shape.getOrigin().y - 1.f)
		 );
	 }

	void LevelRoll::Character::hit()
	 {
		shape.setPosition(LevelRoll::getSizeGrid() * 2,.0f);
		position = shape.getPosition();
		speed = sf::Vector2f(.0f,40.f);
		inAir = true;
		falling = 0;
		line = NULL;
		shape.setRotation(.0f);
		view = sf::View(Game::getWindow()->getDefaultView());
		Game::getWindow()->setView(view);
		hitFirst = false;
		played = false;

		LevelRoll::instance->reset();
		LevelRoll::instance->soundHit->play();
	 }

	LevelRoll::LightController::LightController()
	 {
		setColour(Object::COLOUR_BLACK);
	 }

    void LevelRoll::LightController::onBlack()
     {
		Game::setColourClear(sf::Color::Black);
		*LevelRoll::colourWorld = Object::COLOUR_BLACK;
		dropCharacter();
     }

    void LevelRoll::LightController::onWhite()
     {
		Game::setColourClear(sf::Color::White);
		*LevelRoll::colourWorld = Object::COLOUR_WHITE;
		dropCharacter();
     }

	void LevelRoll::LightController::dropCharacter()
	 {
		if (character->line && character->line->colourOriginal == *LevelRoll::colourWorld)
			character->fall();
	 }

	LevelRoll::Object2::Object2(const Colour &colour)
	:
	colourOriginal(colour)
	 {
	 }

    LevelRoll::Line::Line(const sf::Vector2f &start,const sf::Vector2f &end,const Colour &colour)
	:
	Object2(colour),
	start(start),
	end  (end),
	shape(new sf::RectangleShape(sf::Vector2f(std::abs(pointDistance(start,end)),(right) ? 10.f : -10.f))),
	right(start.x < end.x),
	oppositeAngle(lineAngle((right) ? start : end,(right) ? end : start) + degToRad(180.f)),
	a(std::abs(std::sin(lineAngle(start,end) + degToRad(90.f)))),
	unrestrictedAngle(20.f * a),
	angle(std::min(5.f,20.f * a) * 2.f * ((right) ? 1.f : -1.f))
     {
		shape->setRotation(-(radToDeg(lineAngle(start,end)) - 90.f));
		shape->setPosition(start.x,start.y);
		setColour(colourOriginal);

		if (colourOriginal == Object::COLOUR_BLACK)
			shape->setFillColor(sf::Color::Black);
		else if (colourOriginal == Object::COLOUR_WHITE)
			shape->setFillColor(sf::Color::White);
		else
			shape->setFillColor(sf::Color(128,128,128));
     }

    void LevelRoll::Line::update()
	 {
	 }

    void LevelRoll::Line::draw() const
     {
		if (colourOriginal != *LevelRoll::colourWorld)
			Game::getWindow()->draw(*shape);
     }

	LevelRoll::Line::~Line()
	 {
		delete shape;
	 }

	LevelRoll::Spike::Spike(const float &x,const float &y,const Colour &colour)
	:
	Object2(colour),
	shape(*imageSpike)
	 {
		position.x = x;
		position.y = y;
		shape.setPosition(position);
		setColour(colourOriginal);

		if (colourOriginal == Object::COLOUR_BLACK)
			shape.setColor(sf::Color::Black);
		else if (colourOriginal == Object::COLOUR_WHITE)
			shape.setColor(sf::Color::White);
		else
			shape.setColor(sf::Color(128,128,128));
	 }

	void LevelRoll::Spike::update()
	 {
	 }

	void LevelRoll::Spike::draw() const
	 {
		if (colourOriginal != *LevelRoll::colourWorld)
			Game::getWindow()->draw(shape);
	 }

	const sf::FloatRect LevelRoll::Spike::getGlobalBounds() const
	 {
		sf::FloatRect b(shape.getGlobalBounds());
		b.left += 3;
		b.width -= 6;
		return b;
	 }

	LevelRoll::Coin::Coin(const float &x,const float &y)
	:
	shape(*imageCoin),
	wasHit(false),
	last(false)
	 {
		shape.setOrigin(shape.getLocalBounds().width / 2,shape.getLocalBounds().height / 2);
		shape.setPosition(x,y);
		position = shape.getPosition();
	 }

	void LevelRoll::Coin::update()
	 {
	 }

	void LevelRoll::Coin::draw() const
	 {
		if (!wasHit)
			Game::getWindow()->draw(shape);
	 }

	void LevelRoll::Coin::hit()
	 {
		wasHit = true;
	 }

	LevelRoll::Goal::Goal(const sf::Vector2f &position)
	:
	image("graphics/tileGoal.png"),
	fire("graphics/fire.png")
	 {
		fire.setOrigin(fire.getLocalBounds().width / 2,.0f);
		this->position = position;
	 }

	void LevelRoll::Goal::update()
	 {
	 }

	void LevelRoll::Goal::draw() const
	 {
		for (unsigned i(0); i < ((!LevelRoll::instance->gotAll) ? 11 : 25); ++ i)
		 {
			fire.setScale((static_cast<int>(round(Game::getTimer()->getTimeGlobal() * 4)) % 2) ? 1.f : -1.f,1.f);
			Image *img((LevelRoll::instance->gotAll) ? &image : &fire);
			img->setPosition(position.x + i * img->getLocalBounds().width - ((!LevelRoll::instance->gotAll) ? 20.f : .0f),position.y);
			Game::getWindow()->draw(*img);
		 }
	 }

	const sf::FloatRect LevelRoll::Goal::getGlobalBounds() const
	 {
		sf::FloatRect r;
		r.left = position.x;
		r.top = position.y;
		r.width = Game::getWindow()->getSize().x;
		r.height = Game::getWindow()->getSize().y;
		return r;
	 }

	LevelRoll::Character *LevelRoll::character;
	LevelRoll::Goal *LevelRoll::goal;
	unsigned *LevelRoll::gridSize = new unsigned(20);
	Object::Colour *LevelRoll::colourWorld = new Object::Colour(Object::COLOUR_BLACK);
	Image *LevelRoll::imageSpike, *LevelRoll::imageCoin;
	LevelRoll *LevelRoll::instance;
 }
