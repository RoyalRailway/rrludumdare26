//! @author Adam Emil Skoog, Royal Railway HB
//! @date   2013-04-28

#ifndef __RRLudumDare26__LevelFirst__
	#define __RRLudumDare26__LevelFirst__
	#include "Level.h"
	#include "../../assets/Image.h"
	#include "../../assets/Sound.h"
	#include <SFML/Graphics.hpp>

	namespace flip
	 {
		class LevelFirst : public Level
		 {
			public:
				LevelFirst();
				void update();
				void draw() const;
				bool isDone() const;

			protected:
				void onStart();

			private:
				Image character, floor, ceiling, device, pedestal;
				sf::Text text, textSpace;
				std::string textFull;
				long lengthText;
				mutable unsigned state;
				float yOldDevice, xOldDevice;
				Sound soundWarning, soundText, soundClink;
				sf::Music music1;
		 };
	 }
#endif
