//! @author Adam Emil Skoog, Royal Railway HB
//! @date   2013-04-27

#include "Level.h"
#include "../Object.h"

namespace flip
 {
	Level::Level()
	:
	started(false)
	 {
	 }

	void Level::updateAndDraw()
	 {
		update();
		Object::updateAndDraw();
		draw();
	 }

	void Level::start()
	 {
		started = true;
		onStart();
	 }

	const bool &Level::isStarted() const
	 {
		return started;
	 }
 }
