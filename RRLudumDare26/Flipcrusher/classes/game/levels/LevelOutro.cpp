//! @author Adam Emil Skoog, Royal Railway HB
//! @date   2013-04-30

#include "LevelOutro.h"
#include "../Game.h"
#include "../../../ResourcePath.h"
#include "../Object.h"
#include "../../assets/Sound.h"
#include <cmath>

#ifdef DEBUG
	#include <iostream>
#endif

namespace flip
 {
	LevelOutro::LevelOutro()
	:
	state(0),
	imageMoon("graphics/moon.png"),
	imageCharacter("graphics/characterOutro.png"),
	imageCharacter2("graphics/character32.png"),
	credits("graphics/credits.png")
	 {
		if (!music0.openFromFile(sf::resourcePath() + "sounds/musicOutro0.ogg"))
		 {
			#ifdef DEBUG
				std::cout << "Failed to load sound 'sounds/musicOutro0.ogg'. Aborting execution." << std::endl;
			#endif
			
			exit(EXIT_FAILURE);
		 }

		music0.setLoop(false);

		if (!music1.openFromFile(sf::resourcePath() + "sounds/musicOutro1.ogg"))
		 {
			#ifdef DEBUG
				std::cout << "Failed to load sound 'sounds/musicOutro1.ogg'. Aborting execution." << std::endl;
			#endif
			
			exit(EXIT_FAILURE);
		 }

		music1.setLoop(false);

		imageMoon.setOrigin
		 (
			imageMoon.getLocalBounds().width / 2,
			imageMoon.getLocalBounds().height / 2
		 );
		imageMoon.setPosition
		 (
			Game::getWindow()->getSize().x * .4f +
			imageMoon.getOrigin().x,
			Game::getWindow()->getSize().y * .22f +
			imageMoon.getOrigin().y
		 );

		imageCharacter.setOrigin
		 (
			imageCharacter.getLocalBounds().width / 2,
			imageCharacter.getLocalBounds().height / 2
		 );
		imageCharacter.setPosition(imageMoon.getPosition());
		imageCharacter.setScale(.0f,.0f);

		imageCharacter2.setOrigin
		 (
			imageCharacter2.getLocalBounds().width / 2,
			imageCharacter2.getLocalBounds().height / 2
		 );
		imageCharacter2.setPosition
		 (
			imageCharacter2.getLocalBounds().width * -1.f,
			Game::getWindow()->getSize().y * .5f
		 );

		imageEnemy[0] = new Image("graphics/enemyOutro.png");
		imageEnemy[1] = new Image(*imageEnemy[0]);

		for (unsigned i(0); i < 2; ++ i)
		 {
			imageEnemy[i]->setOrigin
			 (
				imageEnemy[i]->getLocalBounds().width / 2,
				imageEnemy[i]->getLocalBounds().height / 2
			 );
			imageEnemy[i]->setPosition
			 (
				imageCharacter.getPosition().x + 5 * i,
				imageCharacter.getPosition().y
			 );
			imageEnemy[i]->setScale(.0f,.0f);
		 }
	
		credits.setColor(sf::Color(255,255,255,0));
		credits.setPosition(0,0);

		resetStars();
	 }

	void LevelOutro::onStart()
	 {
		Game::setColourClear(sf::Color::Black);
		Game::getTimer()->resetTimeGame();
		Game::getTimer()->resetTimeGlobal();
		music0.play();
	 }

	void LevelOutro::update()
	 {
		if (state < 2)
		 {
			for (unsigned i(0); i < COUNT_STARS; ++ i)
			 {
				const float speed((state == 0) ? -4.f : (-80. + std::min(40.,80.f * Game::getTimer()->getTimeGame() / 6.f)) * 5.f);
				Game::getWindow()->draw(stars[i]);
				stars[i].move(Game::getTimer()->getTimeGlobalDelta() * speed,.0f);

				if (stars[i].getPosition().x < 2)
					stars[i].move(Game::getWindow()->getSize().x / 9.f * 12.f,.0f);
			 }
		 }

		switch (state)
		 {
			case 0:
			 {
				const double t(Game::getTimer()->getTimeGlobal() * .68f);
				const float s(t * (t / 6.f));
				imageCharacter.setScale(s,s);
				imageCharacter.move(-10.f * t * 2.f * Game::getTimer()->getTimeGlobalDelta(),.0f);
				imageMoon.move(10.f * t * 1.4f * Game::getTimer()->getTimeGlobalDelta(),.0f);
				imageMoon.setScale(1.f - s / 22.f,1.f - s / 22.f);

				const float c(255.f - std::max(.0,round((t * 25.f) / 20.f) * 20.f));
				imageMoon.setColor(sf::Color(c,c,c));

				for (unsigned i(0); i < 2; ++ i)
				 {
					imageEnemy[i]->setScale(s * (.4f + .12f * i),s * (.4f + .12f * i));
					imageEnemy[i]->move((1.f + 3.f * i) * t * 1.4f * Game::getTimer()->getTimeGlobalDelta(),.0f);
				 }

				if (imageMoon.getPosition().x > Game::getWindow()->getSize().x + imageMoon.getOrigin().x)
				 {
					Game::getTimer()->resetTimeGlobal();
					Game::getTimer()->resetTimeGame();
					resetStars();

					for (unsigned i(0); i < 2; ++ i)
					 {
						imageEnemy[i]->setScale(-1.f,1.f);
						imageEnemy[i]->setPosition
						 (
							Game::getWindow()->getSize().x * -.4f - 40.f * i,
							Game::getWindow()->getSize().y * (.3f + .3f * i)
						 );
					 }

					++ state;
				 }

				break;
			 }

			case 1:
			 {
				const float m((100.f - std::min(100.,100. * Game::getTimer()->getTimeGame() / 6.f)) * Game::getTimer()->getTimeGlobalDelta());
				imageCharacter2.move(m,.0f);

				for (unsigned i(0); i < 2; ++ i)
					imageEnemy[i]->move(m * .8f,.0f);

				if (music0.getStatus() != sf::Sound::Playing)
				 {
					Object::soundFlip->play();
					++ state;
					Game::setColourClear(sf::Color::White);
					Game::getTimer()->resetTimeGame();
					Game::getTimer()->resetTimeGlobal();
				 }

				break;
			 }

			case 2:
			 {
				if (Game::getTimer()->getTimeGame() > 2.5f)
				 {
					++ state;
					Game::getTimer()->resetTimeGlobal();
					Game::getTimer()->resetTimeGame();
					music1.play();
					Object::soundFlip->play();
					Game::setColourClear(sf::Color::Black);
				 }

				break;
			 }

			case 3:
			 {
				credits.setColor
				 (
					sf::Color
					 (
						255,255,255,
						std::min(255.,round((255. * Game::getTimer()->getTimeGlobal()) / 25.) * 25.)
					 )
				 );

				break;
			 }
		 }
	 }

	void LevelOutro::draw() const
	 {
		if (state == 0)
		 {
			Game::getWindow()->draw(imageMoon);

			for (unsigned i(0); i < 2; ++ i)
				Game::getWindow()->draw(*imageEnemy[i]);

			Game::getWindow()->draw(imageCharacter);

			sf::RectangleShape s(sf::Vector2f(Game::getWindow()->getSize().x,Game::getWindow()->getSize().y));
			s.setPosition(.0f,.0f);
			s.setFillColor
			 (
				sf::Color
				 (
					0,0,0,
					std::max(.0,255.f - round((Game::getTimer()->getTimeGame() * 100.f) / 25.f) * 25.f)
				 )
			 );
			Game::getWindow()->draw(s);
		 }
		else if (state == 1)
		 {
			for (unsigned i(0); i < 2; ++ i)
				Game::getWindow()->draw(*imageEnemy[i]);

			Game::getWindow()->draw(imageCharacter2);
		 }
		else
			Game::getWindow()->draw(credits);
	 }

	void LevelOutro::resetStars()
	 {
		const float w(Game::getWindow()->getSize().x / 9.f),
		            h(Game::getWindow()->getSize().y / 6.f);

		for (unsigned i(0); i < COUNT_STARS; ++ i)
		 {
			stars[i] = sf::RectangleShape(sf::Vector2f(1.f,1.f));
			stars[i].setFillColor(sf::Color::White);
			stars[i].setPosition
			 (
				w + w * (i % 12) + std::rand() % 20 * ((std::rand() % 100 < 50) ? 1.f : -1.f),
				h + h * std::floor(i / 12.f) + std::rand() % 20 * ((std::rand() % 100 < 50) ? 1.f : -1.f)
			 );
		 }
	 }
	
	LevelOutro::~LevelOutro()
	 {
		for (int i(1); i <= 0; -- i)
			delete imageEnemy[i];
	 }
 }
