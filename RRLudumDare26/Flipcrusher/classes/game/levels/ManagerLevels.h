//! @author Adam Emil Skoog, Royal Railway HB
//! @date   2013-04-28

// Updates.
// 2013-04-30

#ifndef __RRLudumDare26__ManagerLevels__
	#define __RRLudumDare26__ManagerLevels__
	#include <SFML/Audio.hpp>

	namespace flip
	 {
		class Level;
		class Image;

		class ManagerLevels
		 {
			public:
				ManagerLevels();
				void updateAndDraw();
				void start();
				~ManagerLevels();

				static sf::Music *music;

			private:
				enum LevelID
				 {
					LEVEL_0,
					LEVEL_1,
					LEVEL_2,
					OUTRO,
					COUNT_LEVELS
				 };

				enum State
				 {
					STATE_BETWEEN,
					STATE_LEVEL
				 };

				Level *levelCurrent;
				unsigned countCleared;
				Image *icons[2][COUNT_LEVELS];
				State state;
				unsigned substate;
				unsigned level;
		 };
	 }
#endif
