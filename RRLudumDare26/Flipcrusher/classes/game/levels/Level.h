//! @author Adam Emil Skoog, Royal Railway HB
//! @date   2013-04-27

#ifndef __RRLudumDare26__Level__
	#define __RRLudumDare26__Level__

	namespace flip
	 {
		class Level
		 {
			public:
				Level();
				void updateAndDraw();
				void start();
				const bool &isStarted() const;
				virtual bool isDone() const { return false; }
				virtual ~Level() {}

			protected:
				virtual void onStart() {};
				virtual void update() {}
				virtual void draw() const {}

			private:
				bool started;
		 };
	 }
#endif
