//! @author Adam Emil Skoog, Royal Railway HB
//! @date   2013-04-27

// Updates:
// 2013-04-28
// 2013-04-29

#include "Object.h"
#include "../assets/Sound.h"

namespace flip
 {
	Object::Object()
	:
	position(.0f,.0f)
	 {
		objects->push_back(this);
	 }

	void Object::initialise()
	 {
		objects = new std::vector<Object *>;
		soundFlip = new Sound("sounds/flip.ogg");
	 }

	void Object::updateAndDraw()
	 {
		for (unsigned i(0); i < objects->size(); ++ i)
		 {
			(*objects)[i]->update();
			(*objects)[i]->draw();
		 }
	 }

	void Object::flipColours(const bool &playSound)
	 {
		if (isFlipEnabled())
		 {
			if (playSound)
				soundFlip->play();

			for (unsigned i(0); i < objects->size(); ++ i)
				(*objects)[i]->flipColour();
		 }
	 }

	void Object::setFlipEnabled(const bool &set)
	 {
		*flipEnabled = set;
	 }

	const bool &Object::isFlipEnabled()
	 {
		return *flipEnabled;
	 }

	void Object::clearObjects()
	 {
		if (objects)
		 {
			for (unsigned i(0); i < objects->size(); ++ i)
				delete (*objects)[i];

			objects->clear();
		 }
	 }

	void Object::deinitialise()
	 {
		clearObjects();
		delete soundFlip;
		delete flipEnabled;
		delete objects;
		objects = NULL;
	 }

	Object &Object::setColour(const Colour &colour)
	 {
		this->colour = colour;

		if (colour == COLOUR_WHITE)
			onWhite();
		else if (colour == COLOUR_BLACK)
			onBlack();
		else
			onNeutral();

		return *this;
	 }

	const Object::Colour &Object::getColour() const
	 {
		return colour;
	 }

	Object &Object::flipColour()
	 {
		if (getColour() == COLOUR_WHITE)
			setColour(COLOUR_BLACK);
		else if (getColour() == COLOUR_BLACK)
			setColour(COLOUR_WHITE);

		return *this;
	 }

	std::vector<Object *> *Object::objects = NULL;
	bool *Object::flipEnabled = new bool(false);
	Sound *Object::soundFlip = NULL;
 }
