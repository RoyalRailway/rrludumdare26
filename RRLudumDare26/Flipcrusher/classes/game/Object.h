//! @author Adam Emil Skoog, Royal Railway HB
//! @date   2013-04-27

// Updates:
// 2013-04-28
// 2013-04-29

#ifndef __RRLudumDare26__Object__
	#define __RRLudumDare26__Object__
	#include <SFML/Graphics.hpp>
	#include <vector>

	namespace flip
	 {
		class Sound;

		class Object
		 {
			public:
				enum Colour
				 {
					COLOUR_NEUTRAL,
					COLOUR_BLACK,
					COLOUR_WHITE
				 };

				explicit Object();
				static void initialise();
				static void updateAndDraw();
				static void flipColours(const bool & = true);
				static void setFlipEnabled(const bool & = true);
				static const bool &isFlipEnabled();
				static void clearObjects();
				static void deinitialise();
				virtual void update() {}
				virtual void draw() const {}
				Object &setColour(const Colour &);
				const Colour &getColour() const;
				Object &flipColour();
				virtual ~Object() {}

				sf::Vector2f position;

			protected:
				virtual void onNeutral() {}
				virtual void onWhite() {}
				virtual void onBlack() {}

			public:
				static Sound *soundFlip;

			private:
				static std::vector<Object *> *objects;
				static bool *flipEnabled;
				Colour colour;
		 };
	 }
#endif
