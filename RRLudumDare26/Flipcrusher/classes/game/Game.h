//! @author Adam Emil Skoog, Royal Railway HB
//! @date   2013-04-27

// Updates:
// 2013-04-28

#ifndef __RRLudumDare26__Game__
	#define __RRLudumDare26__Game__
	#include <SFML/Graphics.hpp>

	namespace flip
	 {
		class Game
		 {
			public:
				class Timer;

				static void run();
				static const unsigned getSizeGrid();
				static const unsigned getRows();
				static const unsigned getColumns();
				static sf::RenderWindow *const getWindow();
				static Timer *const getTimer();
				static sf::Font *const getFont();
				static void setColourClear(const sf::Color &);
				static const sf::Color &getColourClear();
				static const bool &isInputActive();

			private:
				static sf::RenderWindow *window;
				static Timer            *timer;
				static sf::Font         *font;
				static sf::Color        *colourClear;
				static bool             *inputActive;

				static void initialise();
				static void deinitialise();

			public:
				class Timer
				 {
					public:
						Timer();
						const double getTimeGlobal();
						const double getTimeGlobalDelta();
						const double getTimeGame();
						const double getTimeGameDelta();
						Timer &resetTimeGlobal();
						Timer &resetTimeGame();
						Timer &pauseTimeGame(const bool & = true);
						const bool &isGamePaused() const;
						void update();

					private:
						double    timeGlobal,
								  timeGame,
								  timeDelta;
						bool      pausedGame;
						sf::Clock clockTime;

				 };
		 };
	 }
#endif
