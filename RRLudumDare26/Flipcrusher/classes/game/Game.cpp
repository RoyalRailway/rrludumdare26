//! @author Adam Emil Skoog, Royal Railway HB
//! @date   2013-04-27

// Updates:
// 2013-04-28
// 2013-04-29

#include "Game.h"
#include "Object.h"
#include "../Intro.h"
#include "../Menu.h"
#include "../../ResourcePath.h"
#include "levels/ManagerLevels.h"
#include <ctime>

#ifdef DEBUG
	#include <iostream.h>
#endif

namespace flip
 {
	void Game::initialise()
	 {
		std::srand(static_cast<unsigned>(std::time(NULL)));

		window = new sf::RenderWindow
		 (
			sf::VideoMode
			 (
				getSizeGrid() * getColumns(),
				getSizeGrid() * getRows()
			 ),
			"Flipcrusher",
			sf::Style::Titlebar | sf::Style::Close
		 );

		timer  = new Timer;

		Object::initialise();

		font = new sf::Font;

		if (!font->loadFromFile(sf::resourcePath() + "graphics/bm_space.ttf"))
		 {
			#ifdef DEBUG
				std::cout << "Failed to load font 'graphics/bm_space.ttf'. Aborting execution." << std::endl;
			#endif

			exit(EXIT_FAILURE);
		 }
	 }

	const unsigned Game::getSizeGrid()
	 {
		return 16;
	 }

	const unsigned Game::getRows()
	 {
		return 25;
	 }

	const unsigned Game::getColumns()
	 {
		return 25;
	 }

	void Game::run()
	 {
		initialise();
		Intro *intro(new Intro);
		Menu *menu(new Menu);
		ManagerLevels *levels(NULL);

		while (window->isOpen())
		 {
			sf::Event e;

			while (window->pollEvent(e))
			 {
				if (e.type == sf::Event::LostFocus)
				 {
					*inputActive = false;
					getTimer()->pauseTimeGame();
				 }

				if (e.type == sf::Event::GainedFocus)
				 {
					*inputActive = true;
					getTimer()->pauseTimeGame(false);
				 }

				if (e.type == sf::Event::Closed)
					window->close();

				if (isInputActive())
				 {
					if (e.type == sf::Event::KeyPressed)
						if (e.key.code == sf::Keyboard::Space)
							Object::flipColours();
				 }
			 }

			window->clear(getColourClear());
			timer->update();

			if (intro)
			 {
				intro->updateAndDraw();

				if (intro->isDone())
				 {
					delete intro;
					intro = NULL;
					menu->start();
				 }
			 }
			else if (menu)
			 {
				menu->updateAndDraw();

				if (menu->isDone())
				 {
					delete menu;
					menu = NULL;
				 }
			 }
			else
			 {
				if (!levels)
				 {
					levels = new ManagerLevels;
					levels->start();
				 }

				levels->updateAndDraw();
			 }

			window->display();
		 }

		delete levels;

		if (menu)
			delete menu;

		if (intro)
			delete intro;

		deinitialise();
	 }

	sf::RenderWindow *const Game::getWindow()
	 {
		return window;
	 }

	Game::Timer *const Game::getTimer()
	 {
		return timer;
	 }

	sf::Font *const Game::getFont()
	 {
		return font;
	 }

	void Game::setColourClear(const sf::Color &colour)
	 {
		*colourClear = colour;
	 }

	const sf::Color &Game::getColourClear()
	 {
		return *colourClear;
	 }
	
	const bool &Game::isInputActive()
	 {
		return *inputActive;
	 }

	void Game::deinitialise()
	 {
		Object::deinitialise();
		delete inputActive;
		delete colourClear;
		delete font;
		delete timer;
		delete window;
	 }

	Game::Timer::Timer()
	:
	timeGlobal(.0),
	timeGame  (.0),
	pausedGame(false)
	 {
	 }

	const double Game::Timer::getTimeGlobal()
	 {
		return timeGlobal;
	 }

	const double Game::Timer::getTimeGlobalDelta()
	 {
		return (!isGamePaused()) ? ((timeDelta < .05) ? timeDelta : .0) : .0;
	 }

	const double Game::Timer::getTimeGame()
	 {
		return timeGame;
	 }

	const double Game::Timer::getTimeGameDelta()
	 {
		return getTimeGlobalDelta();
	 }

	Game::Timer &Game::Timer::resetTimeGlobal()
	 {
		timeGlobal = .0;
		return *this;
	 }

	Game::Timer &Game::Timer::resetTimeGame()
	 {
		timeGame = .0;
		return *this;
	 }

	Game::Timer &Game::Timer::pauseTimeGame(const bool &set)
	 {
		pausedGame = set;
		return *this;
	 }

	const bool &Game::Timer::isGamePaused() const
	 {
		return pausedGame;
	 }

	void Game::Timer::update()
	 {
		timeDelta   = (!isGamePaused()) ? clockTime.restart().asSeconds() : (clockTime.restart(),.0);
		timeGlobal += timeDelta;
		timeGame   += timeDelta;
	 }

	sf::RenderWindow *Game::window = NULL;
	Game::Timer      *Game::timer  = NULL;
	sf::Font         *Game::font  = NULL;
	sf::Color        *Game::colourClear = new sf::Color(sf::Color::Black);
	bool             *Game::inputActive = new bool(true);
 }
