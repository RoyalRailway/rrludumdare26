//! @author Adam Emil Skoog, Royal Railway HB
//! @date   2013-04-27

#include "Intro.h"
#include "game/Game.h"
#include <cmath>

namespace flip
 {
	Intro::Intro()
	:
	imageLogo("graphics/logo.png"),
	textURL  ("www.royalrailway.com",*Game::getFont(),11),
	state    (0)
	 {
		imageLogo.setOrigin
		 (
			imageLogo.getLocalBounds().width / 2,
			imageLogo.getLocalBounds().height / 2
		 );

		imageLogo.setPosition
		 (
			Game::getWindow()->getSize().x / 2,
			Game::getWindow()->getSize().y / 2
		 );

		imageLogo.setColor(sf::Color(255,255,255,0));

		textURL.setOrigin(textURL.getLocalBounds().width / 2,0);
		textURL.setPosition(imageLogo.getPosition().x,Game::getWindow()->getSize().y * .9f);
	 }

	void Intro::updateAndDraw()
	 {
		const double t(Game::getTimer()->getTimeGlobal()), wait(.2);

		if (state == 0)
		 {
			if (t >= wait)
			 {
				imageLogo.setColor(sf::Color(255,255,255,std::min(255.,255. * (t - wait))));

				if (t >= 1. + wait)
				 {
					++ state;
					Game::getTimer()->resetTimeGlobal();
				 }
			 }
		 }
		else if (state == 1 && t >= 2.2)
		 {
			++ state;
			Game::getTimer()->resetTimeGlobal();
		 }
		else if (state == 2)
		 {
			imageLogo.setColor(sf::Color(255,255,255,255. - std::min(255.,round(255. * t / 25.f) * 25.f)));

			if (t >= 1.)
				state = 3;
		 }

		Game::getWindow()->draw(imageLogo);
		textURL.setColor(imageLogo.getColor());
		Game::getWindow()->draw(textURL);
	 }

	bool Intro::isDone() const
	 {
		return (state == 3);
	 }
 }
