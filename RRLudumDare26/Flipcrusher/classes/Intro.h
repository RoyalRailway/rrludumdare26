//! @author Adam Emil Skoog, Royal Railway HB
//! @date   2013-04-27

#ifndef __RRLudumDare26__Intro__
	#define __RRLudumDare26__Intro__
	#include "assets/Image.h"

	namespace flip
	 {
		class Intro
		 {
			public:
				Intro();
				void updateAndDraw();
				bool isDone() const;

			private:
				Image    imageLogo;
				sf::Text textURL;
				unsigned state;
		 };
	 }
#endif
