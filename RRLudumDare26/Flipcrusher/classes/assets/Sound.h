//! @author Adam Emil Skoog, Royal Railway HB
//! @date   2013-04-27

#ifndef __RRLudumDare26__Sound__
	#define __RRLudumDare26__Sound__
	#include <SFML/Audio.hpp>

	namespace flip
	 {
		class Sound : public sf::Sound
		 {
			public:
				explicit Sound(const std::string & = "");
				explicit Sound(const Sound &);
				~Sound();

			private:
				sf::SoundBuffer *buffer;
				bool copied;
		 };
	 }
#endif