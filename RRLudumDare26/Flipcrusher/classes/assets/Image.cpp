//! @author Adam Emil Skoog, Royal Railway HB
//! @date   2013-04-27

#include "Image.h"
#include "../../ResourcePath.h"

#ifdef DEBUG
	#include <iostream.h>
#endif

namespace flip
 {
	Image::Image(const std::string &path)
	:
	texture(NULL),
	copied (false)
	 {
		texture = new sf::Texture;

		if (!texture->loadFromFile(sf::resourcePath() + path))
		 {
			delete texture;
			texture = NULL;

			#ifdef DEBUG
				std::cout << "Texture '" << path << "' could not be loaded. Aborting execution." << std::endl;
			#endif

			exit(EXIT_FAILURE);
		 }
		else
			setTexture(*texture);
	 }

	Image::Image(const Image &other)
	:
	texture(other.texture),
	copied (true)
	 {
		if (texture)
			setTexture(*texture);
	 }

	Image::~Image()
	 {
		if (texture && !copied)
			delete texture;
	 }
 }
