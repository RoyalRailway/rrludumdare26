//! @author Adam Emil Skoog, Royal Railway HB
//! @date   2013-04-27

#include "Sound.h"
#include "../../ResourcePath.h"

#ifdef DEBUG
	#include <iostream.h>
#endif

namespace flip
 {
	Sound::Sound(const std::string &path)
	:
	buffer(NULL),
	copied(false)
	 {
		buffer = new sf::SoundBuffer;

		if (!buffer->loadFromFile(sf::resourcePath() + path))
		 {
			delete buffer;
			buffer = NULL;

			#ifdef DEBUG
				std::cout << "Sound '" << path << "' could not be loaded. Aborting execution." << std::endl;
			#endif

			exit(EXIT_FAILURE);
		 }
		else
			setBuffer(*buffer);
	 }

	Sound::Sound(const Sound &other)
	:
	buffer(other.buffer),
	copied (true)
	 {
		if (buffer)
			setBuffer(*buffer);
	 }

	Sound::~Sound()
	 {
		if (buffer && !copied)
			delete buffer;
	 }
 }
