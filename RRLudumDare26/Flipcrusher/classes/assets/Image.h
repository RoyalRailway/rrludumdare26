//! @author Adam Emil Skoog, Royal Railway HB
//! @date   2013-04-27

#ifndef __RRLudumDare26__Image__
	#define __RRLudumDare26__Image__
	#include <SFML/Graphics.hpp>

	namespace flip
	 {
		class Image : public sf::Sprite
		 {
			public:
				explicit Image(const std::string & = "");
				explicit Image(const Image &);
				~Image();

			private:
				sf::Texture *texture;
				bool copied;
		 };
	 }
#endif
