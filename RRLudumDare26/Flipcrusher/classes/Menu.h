//! @author Adam Emil Skoog, Royal Railway HB
//! @date   2013-04-27

// Updates:
// 2013-04-28

#ifndef __RRLudumDare26__Menu__
	#define __RRLudumDare26__Menu__
	#include <SFML/Graphics.hpp>
	#include <SFML/Audio.hpp>

	namespace flip
	 {
		class Image;

		class Menu
		 {
			public:
				Menu();
				void updateAndDraw();
				void start();
				bool isDone() const;
				~Menu();

			private:
				enum CountStars
				 {
					COUNT_STARS = 60
				 };

				Image *imageCopyright, *imageLogo, *imagePress;
				sf::RectangleShape stars[COUNT_STARS];
				unsigned state;
				sf::Music music;
		 };
	 }
#endif
