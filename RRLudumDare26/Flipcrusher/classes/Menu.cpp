//! @author Adam Emil Skoog, Royal Railway HB
//! @date   2013-04-27

// Updates:
// 2013-04-28

#include "Menu.h"
#include "game/Game.h"
#include "assets/Image.h"
#include "assets/Sound.h"
#include "../ResourcePath.h"
#include "game/Object.h"
#include <sstream>
#include <cmath>

#ifdef DEBUG
	#include <iostream>
#endif

namespace flip
 {
	Menu::Menu()
	:
	imageCopyright(new Image("graphics/copyrightMenu.png")),
	imageLogo(new Image("graphics/logoGame.png")),
	imagePress(new Image("graphics/pressSpace.png")),
	state(0)
	 {
		if (!music.openFromFile(sf::resourcePath() + "sounds/musicIntro.ogg"))
		 {
			#ifdef DEBUG
				std::cout << "Failed to load sound 'sounds/musicIntro.ogg'. Aborting execution." << std::endl;
			#endif
			
			exit(EXIT_FAILURE);
		 }

		music.setLoop(true);
		music.play();

		imageCopyright->setOrigin
		 (
			imageCopyright->getLocalBounds().width / 2,
			imageCopyright->getLocalBounds().height
		 );

		imageCopyright->setPosition
		 (
			Game::getWindow()->getSize().x / 2,
			Game::getWindow()->getSize().y * .95f
		 );

		const float w(Game::getWindow()->getSize().x / 9.f),
		            h(Game::getWindow()->getSize().y / 6.f);

		for (unsigned i(0); i < COUNT_STARS; ++ i)
		 {
			stars[i] = sf::RectangleShape(sf::Vector2f(1.f,1.f));
			stars[i].setFillColor(sf::Color::White);
			stars[i].setPosition
			 (
				w + w * (i % 12) + std::rand() % 20 * ((std::rand() % 100 < 50) ? 1.f : -1.f),
				h + h * std::floor(i / 12.f) + std::rand() % 20 * ((std::rand() % 100 < 50) ? 1.f : -1.f)
			 );
		 }

		imagePress->setOrigin
		 (
			imagePress->getLocalBounds().width / 2,
			imagePress->getLocalBounds().height / 2
		 );

		imagePress->setPosition
		 (
			Game::getWindow()->getSize().x / 2,
			Game::getWindow()->getSize().y / 2
		 );
	 }

	void Menu::updateAndDraw()
	 {
		if (state > 0)
		 {
			for (unsigned i(0); i < COUNT_STARS; ++ i)
			 {
				Game::getWindow()->draw(stars[i]);
				stars[i].move(Game::getTimer()->getTimeGlobalDelta() * -40.f,.0f);

				if (stars[i].getPosition().x < 2)
					stars[i].move(Game::getWindow()->getSize().x / 9.f * 12.f,.0f);
			 }

			imageLogo->setPosition
			 (
				0,
				24.f + std::sin(Game::getTimer()->getTimeGlobal() * 3.) * 3.f
			 );

			imagePress->setPosition
			 (
				imagePress->getPosition().x,
				Game::getWindow()->getSize().y / 2.f + 30.f +
				std::sin(Game::getTimer()->getTimeGlobal() * 3.) * -1.f
			 );

			imagePress->setColor
			 (
				sf::Color
				 (
					255,255,255,
					155.f + 100.f * std::sin(Game::getTimer()->getTimeGlobal() * 8.)
				 )
			 );

			Game::getWindow()->draw(*imageLogo);
			Game::getWindow()->draw(*imageCopyright);
			Game::getWindow()->draw(*imagePress);

			if (state == 1)
			 {
				sf::RectangleShape s(sf::Vector2f(Game::getWindow()->getSize().x,Game::getWindow()->getSize().y));
				s.setPosition(.0f,.0f);
				s.setFillColor(sf::Color(0,0,0,255.f - std::min(255.,round(255. * Game::getTimer()->getTimeGlobal() / 25.f) * 25.f)));
				Game::getWindow()->draw(s);

				if (Game::isInputActive())
				 {
					if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
					 {
						state = 2;
						Game::getTimer()->resetTimeGame();
						Object::soundFlip->play();
					 }
				 }
			 }
			else if (state == 2)
			 {
				sf::RectangleShape s(sf::Vector2f(Game::getWindow()->getSize().x,Game::getWindow()->getSize().y));
				s.setPosition(.0f,.0f);
				s.setFillColor(sf::Color(0,0,0,std::min(255.,round(255. * Game::getTimer()->getTimeGame() / 25.f) * 25.f)));
				Game::getWindow()->draw(s);

				if (Game::getTimer()->getTimeGame() >= 1.)
					state = 3;
			 }
		 }
	 }

	void Menu::start()
	 {
		state = 1;
		Game::getTimer()->resetTimeGlobal();
	 }

	bool Menu::isDone() const
	 {
		return (state == 3);
	 }

	Menu::~Menu()
	 {
		delete imagePress;
		delete imageLogo;
		delete imageCopyright;
	 }
 }
